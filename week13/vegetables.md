# Selected Questions, *Kurose* Chapter Six and Seven, Sections 6.4, 6.7, and 7.3

## Suppose nodes A, B, and C each attach to the same broadcast LAN. If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C's adapter process these frames? If so, will C's adapter pass the IP datagrams in these frames to network layer C? How would your answers change if A sends frames with the MAC broadcast address?
In the case that node A uses node B's MAC address in each frame, then node C's adapter will process the frames from node A, but will not pass the IP datagrams in these frames to the C's network layer. On the other hand, in the case that Node A uses the MAC broadcast address, then node C's adapter will both process the frames from node A and pass the IP datagrams in these frames to C's network layer.

## Why is an ARP query message sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?
ARP query messages are sent within a broadcast frame because the sending host does not yet know the MAC address of the destination host, and the broadcast MAC address ensures it reaches that destination host. When the destination host receives the ARP query message and subsequently sends the ARP response message, by using a fram with the destination host's specific MAC address, the original sending host can actualy obtain the MAC address it was quering for.

## Consider three LANs interconnected by two routers, as shown in Figure 6.33.
**a) assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx, for Subnet 2 use addresses of the form 192.168.2.xxx, and for Subnet 3 use addresses of the form 192.168.3.xxx.**
| Interface | IP address  |
|-----------|-------------|
| A         | 192.168.1.1 |
| B         | 192.168.1.2 |
| C         | 192.168.1.1 |
| D         | 192.168.1.2 |
| E         | 192.168.1.1 |
| F         | 192.168.1.2 |

**b) Assign MAC addresses to all of the adapters.**
| Interface | MAC address |
|-----------|-------------|
| A         | 0:0A        |
| B         | 0:0B        |
| C         | 0:0C        |
| D         | 0:0D        |
| E         | 0:0E        |
| F         | 0:0F        |

**c) Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single router example in Section 6.4.1.**
First, host E will create an IP datagram with its IP address as the source IP address and host B's IP address as the destination IP address. Then host E will send this IP datagram down to its adapter and tell it that the source MAC address is its own MAC address and the destination MAC address is R2's MAC address via its ARP table. This ensures that the frame is sent to R2, which then takes the frame and passes it up to its network layer and determines via its forwarding table that the IP datagram from host E needs to be interfaced through Subnet 2 to Subnet 1 and creates a new frame which is destined with R1's MAC address via its ARP table. Again, R1 will receive the frame from R2 and pass it up to its network later that determines via its forwarding table that the IP datagram from host E needs to be interfaced to host B and creates a new frame destined with host B's MAC address via its ARP table. Finally, host B receives the frame from R1 and passes it up to host B's network layer which takes appropriate action with host B's IP datagram.

**d) Repeat (c), now assuming that the ARP table in the sending host is empty.** Now, in this case, all the above steps are all the same, the only step that changes is the very first step. More specifically, host E must send an ARP query message to query R2's MAC address so that it is able to send its frame to R2. Once, host E receives R2's MAC address, it will update its ARP table and send the frame to R2.

## Consider Figure 6.33. Now replace the router between Subnets 1 and 2 with a switch S1, and label the router between Subnets 2 and 3 as R1.
**a) Consider sending an IP datagram from host E to host F. Will host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?** No, because host E and F in the same Subnet. Source IP/MAC address: host E IP address/host E MAC address. Destination IP/MAC address: host F IP address/host F MAC address.

**b) Suppose host E would like to send an IP datagram to host B, and assume that host E's ARP cache does not contain host B's MAC address. Will host E perform an ARP query to find host B's MAC address? Why? In the Ethernet frame that is delivered to router R1, what are the source and destination IP and MAC addresses?** No, but host E will send an ARP query for router R1's MAC address because router R1 is the first hop to host B. Source IP/MAC address: host E IP address/host E MAC address. Destination IP/MAC address: host B IP address/router R1 MAC address.

**c) Suppose host A would like to send an IP datagram to host B, and neither host A's ARP cache contains host B's MAC address nor does host B's ARP cache contain host A's MAC address. Further suppose that the switch S1's forwarding table contains entries for host B and router R1 only. Thus, host A will broadcast an ARP query message. What actions will switch S1 perform once it receives the ARP query message? Will router R1 receive this ARP query message? If so, will router R1 forward the message to Subnet 3? Once host B receives this ARP query message, it will send back to host A an ARP response message. But will it send an ARP query message to ask for host A's MAC address? Why? What will switch S1 do once it receives an ARP response message from host B?** Switch S1 will simply forward the ARP query message from host A on both its interfaces and switch S1 will where host A resides and add host A to its forwarding table. Yes, router R1 will receive the ARP query message from host A, but will not forward that ARP query message. When host B sends the ARP response message, it will not hace to send an ARP query message as it received host A's MAC address from the ARP query message. When switch S1 receives the ARP response message from host B, it will add host B to its forwarding table and forward the ARP response messge to host A.

## What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?
Path loss is simply when a given signal disperses resulting in decreased signal strength over the distance the signal travels. Multipath propagation is where parts of a signal travel different paths causing them to arrive at different times and can change over time from moving objects between the sender and receiver. Finally, interference is caused by other sources transmitting on the same frequency or simply can be caused by noise from the enviroment.

## As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increace?
First, the base station can increase its transmission power to increase the SNR (signal-to-noise ratio), which in turn lowers the BER (bit error rate). Secondly, the base station can simply decrease its transmission rate, which can reasonbly be inferred will also decrease the BER.

## In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately?
Put simply, I would say that the designers of the CSMA/CA protocol designed their protocol with the idea of fairness in mind. More specifically, rather than one station hogging the channel for the duration of their frame transmissions, the CSMA/CA protocol allows for other stations the chance to transmit their frames.

## Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1500 bytes of data, and all other stations are idle a this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgement.
First, we will denote $t_{RTS}$ and $t_{CTS}$ as the time required to transmit the RTS and CTS, respecitively. We will also denote $t_{data}$ as the time required to transmit the 1500 bytes of data. Finally, we will denote $t_{ack}$ as the time required to transmit the acknowledgement. Therefore, we have that the $t_{total} = DIFS + t_{RTS} + SIFS + t_{CTS} + SIFS + t_{data} + SIFS + t_{ack}$.