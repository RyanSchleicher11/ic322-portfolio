# Wireshark ARP Lab

## Process
I completed the ARP Wireshark Lab provided from the Textbook's website. At the time of the lab, I did not have access to Ethernet to run the Wireshark traces properly, so I turned to the provided Ethernet Wireshark trace from the website in my answers to the lab questions.

## Lab Questions
### Ethernet
#### What is the 48-bit Ethernet address of your computer?
After looking at the HTTP GET request packet, I found that the source MAC address was c4:41:1e:75:b1:52.

#### What is the 48-bit destination address in the Ethernet frame? Is this the Ethernet address of gaia.cs.umass.edu? (Hint: the answer is no). What device has this as its Ethernet address?
Again, after looking at the HTTP GET request packet, I found that the destination MAC address was 00:1e:c1:7e:d9:01. And based off of the hint, this MAC address is not the MAC address of gaia.cs.umass.edu. Rather, this is the MAC address of the link used to get off the subnet.

#### What is the hexadecimal value for the two-byte Frame type field in the Ethernet frame carrying the HTTP GET request? What upper layer protocol does this correspond to?
The hexadecimal value for the Frame type field is 0x0800, which corresponds to the IP protocol. This makes sense as we are dealing with HTTP over the Internet which used the IP protocol.

#### How many bytes from the very start of the Ethernet frame does the ASCII “G” in “GET” appear in the Ethernet frame?
I found that the ASCII "G" came after the 58th byte from the start of the Ethernet frame. I confirmed by findings as the length of the Ethernet frame to the data field is 14 bytes long (starting at the destination address), the IP header was 24 bytes long, and the TCP header was 20 bytes long, which sum to 58 bytes.

#### What is the value of the Ethernet source address? Is this the address of your computer, or of gaia.cs.umass.edu (Hint: the answer is no). What device has this as its Ethernet address?
Now looking at the HTTP response packet, I found that the source MAC address was 00:1e:c1:7e:d9:01. Which again by the hint is not the MAC address of gaia.cs.umass.edu, but rather is the link used to get onto the subnet.

#### What is the destination address in the Ethernet frame? Is this the Ethernet address of your computer?
Again, after looking at the HTTP response packet, I found that the destination MAC address was c4:41:1e:75:b1:52, which is the MAC address of the computer used in the provided Wireshark trace.

#### Give the hexadecimal value for the two-byte Frame type field. What upper layer protocol does this correspond to?
The hexadecimal value for the Frame type field is 0x0800, which again corresponds to the IP protocol.

#### How many bytes from the very start of the Ethernet frame does the ASCII “O” in “OK” (i.e., the HTTP response code) appear in the Ethernet frame?
I found that the ASCII "O" came after the 58th byte from the start of the Ethernet frame. Again, I confirmed by findings as the length of the Ethernet frame to the data field is 14 bytes long (starting at the destination address), the IP header was 24 bytes long, and the TCP header was 20 bytes long, which sum to 58 bytes.

#### How many Ethernet frames (each containing an IP datagram, each containing a TCP segment) carry data that is part of the complete HTTP “OK 200 ...” reply message?
After looking at the HTTP response packet, I found that there were four reassembled packets, which I assume means that there were four Ethernet frames that carried data for the complete HTTP 200 OK response.

### ARP
#### How many entries are stored in your ARP cache?
I only had a single entry on my ARP cache, very disappointing.

#### What is contained in each displayed entry of the ARP cache?
From what I can tell, there are a few things contained in each entry of the ARP cache. First the name of the host/router. Second is the IP address of the host/router. Third is the MAC address of the host/router. And lastly is the medium of the connection, but I am not exactly sure.

#### What is the hexadecimal value of the source address in the Ethernet frame containing the ARP request message sent out by your computer?
After looking at the mutlitple ARP query messages sent, I found that the source MAC address in the Ethernet frames was 00:1e:c1:7e:d9:01.

#### What is the hexadecimal value of the destination addresses in the Ethernet frame containing the ARP request message sent out by your computer? And what device(if any) corresponds to that address?
Again, after looking at the multiple ARP query messages, the destination MAC address in the Ethernet frames was ff:ff:ff:ff:ff:ff, or the broadcast MAC address. And the broadcast MAC address signifies all the connected hosts on the subnet.

#### What is the hexadecimal value for the two-byte Ethernet Frame type field. What upper layer protocol does this correspond to?
Again, after looking at the mutliple ARP query messages, the type value in the Ethernet frames was 0x0806 which coresponds to ARP-not a surprise whatsoever.

#### How many bytes from the very beginning of the Ethernet frame does the ARP opcode field begin?
After finding the ARP specification and the Ethernet Frame, I found that there are 20 bytes from the beginning of the Ethernet frame to the beginning of the ARP opcode.

#### What is the value of the opcode field within the ARP request message sent by your computer?
After looking at the first ARP query messages, I found that the ARP opcode was 0x0001 which translates to ARP request and makes sense as I am looking at an ARP query/request message.

#### Does the ARP request message contain the IP address of the sender? If the answer is yes, what is that value?
Again, after looking at the first ARP query messages, I found that they do contain the IP address of the sender which is 128.119.247.1.

#### What is the IP address of the device whose corresponding Ethernet address is being requested in the ARP request message sent by your computer?
Again, after looking at the first ARP query messages, I found that the IP address corresponding to the Ethernet address being queried/requested was 128.119.247.77.

#### What is the value of the opcode field within the ARP reply message received by your computer?
Now looking at the ARP response message that links to the first ARP query message, I found that the ARP opcode value was 0x0002 which translates to ARP reply and makes sense as I am looking at an ARP response message.

#### Finally (!), let’s look at the answer to the ARP request message! What is the Ethernet address corresponding to the IP address that was specified in the ARP request message sent by your computer?
Again, after looking at the ARP response message, I found that the Ethernet address of corresponding to the IP address specified in the original ARP query message was 00:1e:c1:7e:d9:01.

#### We’ve looked the ARP request message sent by your computer running Wireshark, and the ARP reply message sent in response. But there are other devices in this network that are also sending ARP request messages that you can find in the trace. Why are there no ARP replies in your trace that are sent in response to these other ARP request messages?
Because ARP query messages are destined to the MAC broadcast address (or all connected hosts on the subnet), and ARP response messages are specifically destined to the original sending host's MAC address. Therefore, we will not receive other ARP response messages.