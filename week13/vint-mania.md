# VintMania

## "One of the fathers of the Internet"
Vint Cerf is the co-designer, with Robert Kahn, of the TCP/IP protocols and the architecture of the Internet. During his tenure from 1976-1982 with the U.S. Department of Defense's Advanced Research Projects Agency (DARPA), Cerf played a key role leading the development of Internet and Internet-related packet data transport and security technologies.

## Three Surprising Facts About Vint Cerf
1. Vint Cerf and his wife Sigrid Cerf both have hearing deficiencies, in fact they met at a hearing aid agent's practice in the 1960s
2. Vint Cerf worked as assistant professor at Stanford University from 1972 to 1976 where he conducted research and co-designed the TCP/IP protocol with Robert Kahn
3. Vint Cerf helped fund and establish ICANN, the Internet Corporation for Assigned Names and Numbers

## Two Insightful Questions
1. What prompted you to develop the TCP/IP protocols? Was there a problem laid out before you? Or did the idea come to you through research? I believe this question is insightful because I have always been curious how people like Vint Cerf and other prominate Computer Scientists even begin to think of their inventions.
2. What is one thing we should be doing right now as young adults in Computer Sceince to develop ourselves to become successful members of the Computer Science community after our service committment? I beleive this question is insightful because it is specifically geared towards us Midshipmen who want to utilize our Computer Science skillset/degree after our time in the military.