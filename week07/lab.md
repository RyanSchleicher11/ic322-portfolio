# DHCP Wireshark Lab

## Process
I completed the DHCP Wireshark Lab provided from the Textbook's website and did not deviate from the instructions.

## Lab Questions
### Is this DHCP Discover message sent out using UDP or TCP as the underlying transport protocol?
Upon looking into the DHCP Discover message, I have found that the message was sent using the UDP protocol. The DHCP Discover utilizes UDP because a two-way TCP connection cannot even be established before I have been assigned an IP address, therefore, UDP is far simpler and more efficient in this case.

### What is the source IP address used in the IP datagram containing the Discover message? Is there anything special about this address? Explain.
The source IP address contained within the DHCP Discover message is 0.0.0.0. This IP address is special as it essentially is the 'null' address (i.e. the computer can temporarily utilize this address to communicate before being assigned their valid IP address).

### What is the destination IP address used in the datagram containing the Discover message. Is there anything special about this address? Explain.
The destination IP address contained within the DHCP Discover message is 255.255.255.255. This IP address is also special as it acts as the IP address for all connected hosts on that network, therefore, the DHCP Discover message can eventually reach the DHCP server as the its specific IP address is not known yet.

### What is the value in the transaction ID field of this DHCP Discover message?
The transaction ID value contained within the DHCP Discover message was set to 0x652a66c9 (1697277641 in decimal). The transaction ID is used simply to associate messages between the connected host and the DHCP server, as multiple messages may be picked up over the course of the DHCP process.

### Now inspect the options field in the DHCP Discover message. What are five pieces of information (beyond an IP address) that the client is suggesting or requesting to receive from the DHCP server as part of this DHCP transaction?
Upon reviewing the options field of the DHCP Discover message, besides requesting for an IP address, the other things that my host also requested for include: client identifier, host name, vendor class identifier, and parameter request list.

### How do you know that this Offer message is being sent in response to the DHCP Discover message you studied in questions 1-5 above?
I would say that I know the DHCP Offer message that I received was in response to my host's DHCP Discover message is because the two transaction ID's match.

### What is the source IP address used in the IP datagram containing the Offer message? Is there anything special about this address? Explain.
The source IP address contained within the DHCP Offer message is 10.1.74.10. This is kind of special in that this is the IP address of the DHCP server itself.

### What is the destination IP address used in the datagram containing the Offer message? Is there anything special about this address? Explain.
The destination IP address contained within the DHCP offer message is 255.255.255.255. As explained above, this IP address is special as it aacts as the IP address for all connected hosts on the network, which is why my host received the DHCP Offer message.

### Now inspect the options field in the DHCP Offer message. What are five pieces of information that the DHCP server is providing to the DHCP client in the DHCP Offer message?
Upon reviewing the options field of the DHCP Offer messge, I found that the DHCP server is providing my client these bits of information: DHCP server identifier, IP address lease time, subent mask, domain name server, and domain name.

### What is the UDP source port number in the IP datagram containing the first DHCP Request message in your trace? What is the UDP destination port number being used?
Upon reviewing the DHCP Request message, I found that the source port number is 68 and the destination port number is 67. Both port number 67 and 68 are important, as the UDP port number 68 signifies the DHCP client (my host), and the UDP port number 67 signifies the DHCP server.

### What is the source IP address in the IP datagram containing this Request message? Is there anything special about this address? Explain.
The source IP address contained within the DHCP Request message is 0.0.0.0. As explained above, this address is significant as it can be used as a temporary IP address to send messages, as my host has not been assigned a valid IP address just yet.

### What is the destination IP address used in the datagram containing this Request message. Is there anything special about this address? Explain.
The destination IP address contained within the DHCP request message is 255.255.255.255. As explained above, this address is significant as it acts as an IP address for all connected hosts on the network, therefore, the message can eventually reach the DHCP server. Although, this is weird because my host should at this point know the DHCP server's IP address as it received a DHCP Offer message with an option containing the DHCP server's IP address.

### What is the value in the transaction ID field of this DHCP Request message? Does it match the transaction IDs of the earlier Discover and Offer messages?
The transaction ID value contained within the DHCP Request message is 0x652a66c9 (1697277641 in decimal), which does match the transaction IDs of the earlier DHCP Discover and Offer messages.

### Now inspect the options field in the DHCP Discover message and take a close look at the “Parameter Request List”. What differences do you see between the entries in the ‘parameter request list’ option in this Request message and the same list option in the earlier Discover message?
Upon reviewing both the DHCP Request and Discover messages' option field, specifically the 'parameter request list', I could not find any differences between the two. Put simply, I am not sure why that is are what difference I was supposed to see, if any.

### What is the source IP address in the IP datagram containing this ACK message? Is there anything special about this address? Explain.
The source IP address contained within the DHCP ACK message is 10.1.74.10. As explained above, this is IP address of the DHCP server.

### What is the destination IP address used in the datagram containing this ACK message. Is there anything special about this address? Explain.
The destination IP address contained within the DHCP ACK message is 255.255.255.255. As explained many times now, this address is significant as it acts as the address of all connected hosts on the network, which is why my host was able to receive the message.

### What is the name of the field in the DHCP ACK message (as indicated in the Wireshark window) that contains the assigned client IP address?
Upon reviewing the DHCP ACK message, I found that the field which contains my host's assigned IP addres is '''Your (client) IP address: 10.25.134.241'''. And, upon double checking what I found, I ran the command '''ipconfig''' on my laptop's command prompt and found that the IPv4 address from this command matched that of what I found from the DHCP ACK message field.

### For how long a time (the so-called “lease time”) has the DHPC server assigned this IP address to the client?
Upon reviewing the DHCP ACK message, specifically within the options field, I found that the lease time of my host's IP address to be 691200 seconds, or 8 days.

### What is the IP address (returned by the DHCP server to the DHCP client in this DHCP ACK message) of the first-hop router on the default path from the client to the rest of the Internet?
Upon reviwing the DHCP ACK message, I found that the address for the first-hop router from my host to the Internet is 10.25.128.1. I found this address in two places, the '''router''' option within the options field, and the '''relay agent IP address''' field.
