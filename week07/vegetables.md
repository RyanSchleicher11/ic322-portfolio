# Selected Questions, *Kurose* Computer Networks, Chapter Four

## Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).
This is not unique just to input ports, but packet loss may occur when the queue size (determined by the traffic load, relative speed of the switching fabric, and the line speed) grows large enough such that it exhausts the router's buffer space. And in the case of input ports, packet loss may be prevelant in the case of slower switch fabric speeds as compared to the input line speeds However, packet loss may be eliminated if the switching transfer rate (speed), or Rswitch, is at least *N* times faster than the input line speed, or Rline, where *N* is the number of input ports/lines.

## Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?
As mentioned above, packet loss is caused by the overflow of a router's buffer space. And in the case of output ports, packet loss may be found when the switch fabric speeds are faster as compared to the output line speeds. Therefore, increasing the switch fabric speed will only further cause packet loss than prevent it.

## What is HOL blocking? Does it occur in input ports or output ports?
Head-of-the-line (HOL) blocking is the phenomena in which a queued packet in an input queue must wait for transfer through the fabic, even if its output port is free, because it is being blocked by a packet at the head of the line, and can eventually lead to packet loss. Because HOL blocking pertains to input queues, it is clear that HOL blocking occurs in input ports rather than output ports.

## What is an essential different between RR and WFQ packet scheduling? Is there a case where RR and WFQ will behave the exact same?
For both Round Robin (RR) and Weighted Fair Queuing (WFQ) packet scheduling, packets are sorted into classes as with priority queueing. In the case of RR packet scheduling, rather than a strict priority amongst the classes, the RR packet scheduler alternates service amongst the classes, giving each class equal priority. While on the other hand, with WFQ packet scheduling, each class may receive a differential amount of service in a given time interval. Consequently, if each class were to receive the same weight in the case of WFQ packet scheduling, then RR and WFQ packet scheduling will function in the same manner.

## What field in the IP header can be used to ensure that a packet is forwarded through no more than *N* routers?
The time-to-live field. This is because the time-to-live field is decremented by one each time the packet is processed by a router. And, if a given router reads processes a packet with time-to-live field to 0, then that router will drop said packet. Therefore, for a given time-to-live value of *N*, then that packet will be forwareded through no more than *N* routers before it either reaches its destination or is dropped.

## Do routers have IP addresses? If so, how many?
Yes, in fact routers have two IP addresses, one for each of its interfaces (WAN and LAN). The WAN (Wide Area Network) interface is the side of the router that faces the Internet and therefore has a public IP address. Then, the LAN (Local Area Network) interface is the side of the router that faces the home network and therefore has a private IP address.

## Consider the switch shown below. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?
What I have found is that for both the best and worst case scheduling order, the number of time slots needed will be three because of the assumption that a non-empty input queue is never idle. As can be seen, the first time slot will always consist of sending the X from the topmost input queue and a Y from either the middle or bottommost input queue. Then, in the second time slot, we again must always send two packets, a Y from either the middle or bottommost input queue and either an X from the middle input queue or a Z from the bottommost input queue depending on what input port the Y packet came from in the first time slot. Finally, for the third time slot, we must always send the remaining X or Z packet.

## Suppose that the WFQ scheduling policy is applied to a buffer that supports three classes and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.
**a) Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights?** Here is an order to achieve the prescribed WFQ weights: 1123 1312 ... (or any 4 digit combination consisting of two 1s, one 2, and one 3)

**b) Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?** Now, in this case, an order to achieve the prescribed WFQ weights and given situation will look fairly similar to the above part, just exclude the class 3 packets. For example: 112 121 211 ...

## Consider a datagram network using 32-bit host addresses. Suppose a router has four linked, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows:
**a) Provide a forwarding table that has five entries, uses longest prefix matching, and forwards to the correct link interfaces.**
| Destination Address Range                                                 | Interface | Prefix Match      |
|---------------------------------------------------------------------------|-----------|-------------------|
| 11100000 00000000 00000000 00000000 - 11100000 00111111 11111111 11111111 | 0         | 1110              |
| 11100000 01000000 00000000 00000000 - 11100000 01000000 11111111 11111111 | 1         | 11100000 01000000 |
| 11100000 01000001 00000000 00000000 - 11100001 01111111 11111111 11111111 | 2         | 11100001 01       |
| otherwise                                                                 | 3         | otherwise         |

**b) Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses 11001000 10010001 01010001 01010101, 11100001 01000000 11000011 00111100, and 11100001 10000000 00010001 01110111.** For the address 11001000 10010001 01010001 01010101, it would fall under the otherwise category and would go to link interface 3. Next, the address 11100001 01000000 11000011 00111100 fits the 11100001 01 prefix match, therefore would go to link interface 2. Finally, for the address 11100001 10000000 00010001 01110111, it would again fall under the otherwise category and would go to link interface 3.

## Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table. For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.
| Prefix Match | Interface | Destination Address Range | Number of Addresses |
|--------------|-----------|---------------------------|---------------------|
| 00           | 0         | 00000000  -  00111111     | 2^6 = 64            |
| 010          | 1         | 01000000  -  01011111     | 2^5 = 32            |
| 011          | 2         | 01100000  -  01111111     | 2^5 = 32            |
| 10           | 2         | 10000000  -  10111111     | 2^6 = 64            |
| 11           | 3         | 11000000  -  11111111     | 2^6 = 64            |

## Consider a router that interconnects three subnets: Subnet 1, 2, and 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses that satisfy these constraints.
Subnet 1 needs to accomodate at least 60 interfaces, which can be expressed using 6 bits or 2^6 = 64 addresses. Subnet 2 needs to accomodate at least 90 interfaces, which can be expressed using 7 bits or 2^7 = 128 addresses. Subnet 3 needs to accomodate 12 interfaces, which can be expressed using 4 bits or 2^4 = 16 addresses. Therefore, Subnet 1 gets 223.1.17.0/25 (223.1.17.0 through 223.1.17.63), Subnet 2 gets 223.1.17.64/26 (223.1.17.64 through 223.1.17.191), and Subnet 3 gets 223.1.17.192/28 (223.1.17.192 through 223.1.17.207).