# Learning Objectives

## I can explain what a subnet is, how a subnet mask is used, and how longest prefix matching is used to route datagrams to their intended subnets.
As it can be inferred from the name, a subnet is a sub-network, a network inside of a network. A subnet too is an interconnected network containing routers and hosts. In short, here is a general definition given from the book:

> *To determine the subnets, detach each interface from its host or router, creating islands of isolated networks, with interfaces terminating the end points of the isolated networks. Each of these isolated networks is called a subnet.*

But the role of the subnet is to make networks more efficient in how they are designed and traversed. Say we want to route data to a host. In a class A network, there could be millions of connected hosts to search from, and it could very likely take a long time to find the right host. But with subnets, and their associated subnet mask, the range of connected hosts to search from will be reduced dramatically.

In short, all subnets have an associated subnet mask. Essentially, the subnet mask distinguishes the network and the connected hosts within said subnet. For example, given the subnet 211.11.7.0/24, we know that this IPv4 address's first 24 bits are reserved for the network prefix (233.11.7), and the last 8 bits are reserved for host addressing (233.11.7.0 through 233.11.7.255).

Say a router has just received a packet that needs to be forwarded along its route. The router would use its forwarding table to look up the output port to which this packet will be forwarded to via the switch fabric. A router's forwarding table could have any number of destination addresses and their associated link number, and one way the router utilizes its forwarding table is by matching the prefix of the packet's destination address with the entries in its forwarding table. If there are multiple matches for a look up, the router will forward the packet to the link of the match with the longest prefix.


## I can step though the DHCP protocol and show how it is used to assign IP addresses.
The Dynamic Host Configuration Protocol (DHCP) is what is known as a plug-and-play or zero-configuration protocol, in other words, DHCP allows a host of a network/organization to be allocated an IP address automatically. Furthermore, DHCP allows a given host to learn additional information, such as its subnet mask, address of its default gateway, and/or the address of its local DNS server. For a newly connecting host, they will undergo a four-step process with the DHCP protocol:

1. DHCP server discovery: a newly connected host must find a DHCP server to interact with, which is done via a DHCP discover message, or a UDP packet that the host sends to port 67, with a source address of 0.0.0.0, a destination address of 255.255.255.255 (address to send packet to all connected hosts on the subnet), and a unique transaction ID number
2. DHCP server offer(s): the DHCP server response to a DHCP discover message, which contains the DHCP server's source address, a destination address of 255.255.255.255, the unique transaction ID number, and the IP address lease time (it is common for the time to be set to several hours or days), it may be the case that the newly connected host receive mutiple DHCP server offers
3. DHCP request: the newly connected host will choose amongst its DHCP server offers and respond to the selected offer via a DHCP request message, simply echoing back the configuration parameters
4. DHCP ACK: the DHCP server response to the DHCP request message, and the final confirmation of the requested configuration parameters

> ![DHCP process picture](<learning-objectives-image/dhcp-process.gif>)
