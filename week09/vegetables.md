# Selected Questions, *Kurose* Chapter Five, Sections 5.1 through 5.3

## What is the "count to infinity" problem in distance vector routing?
Say a link cost increases between two nodes in a given routing path, then a packet along that path will be caught in the middle of nodes determining thier new least-cost path, or what is known as a routing loop. Essentially, because the distance vector algorithm is iterative and the only information a given node may have is the direct cost to its neighbors, the nodes in the routing loop will incorrectly calculate the least-cost path to the destination node and feed said incorrect least-cost calculations to their neighbor nodes, slowly counting towards infinity. This count to infinity problem can cause significant delay to packets caught in the middle. However, a technique known as route poisoning is a fix to this count to infinity problem.

## True or False: When an OSPF route sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.
False, with OSPF, a router broadcasts routing information to all other routers in the autonomous system, not just its neighboring routers. A router broadcasts link state information whenever there is a change in a link's state. Routers also broakcasts link state information periodically, at least every half hour, even if the link's state has not changed.

## What is meant by an area in an OSPF autonomous system? Why was the concept of an area introduced?
An OSPF autonomous system can be configured hierarchically into areas, where each area runs its own OSPF link state routing algorithm with each router in a given area broadcasting its link state to all other routers in the given area. Within each area there contains at least one area border router that is responsible for routing packets outside the area. Additionally, there is exactly one backbone area within a given autonomous system that is responsible for routing traffic between the other areas within the given autonomous system. In essence, areas are a way to cut down the size of a link state database and traffic within a given autonomous system.

## Consider the following network. With the indicated link costs, use Dijkstra's algorithm to compute the shortest path from *x* to all network nodes.
| step | N'      | D(z),p(z) | D(y),p(y) | D(w),p(w) | D(v),p(v) | D(u),p(u) | D(t),p(t) |
|------|---------|-----------|-----------|-----------|-----------|-----------|-----------|
| 0    | x       | 8,x       | 6,x       | 6,x       | 3,x       | ∞         | ∞         |
| 1    | xv      | 8,x       | 6,x       | 6,x       |           | 6,v       | 7,v       |
| 2    | xvy     | 8,x       |           | 6,x       |           | 6,v       | 7,v       | 
| 3    | xvyw    | 8,x       |           |           |           | 6,v       | 7,v       | 
| 4    | xvywu   | 8,x       |           |           |           |           | 7,v       | 
| 5    | xvywut  | 8,x       |           |           |           |           |           |
| 6    | xvywutz |           |           |           |           |           |           |  

## Consider the network shown above. Using Dijkstra's algorithm, do the following:
**a) Compute the shortest path from *v* to all network nodes.**
| step | N'      | D(z),p(z) | D(y),p(y) | D(w),p(w) | D(x),p(x) | D(u),p(u) | D(t),p(t) |
|------|---------|-----------|-----------|-----------|-----------|-----------|-----------|
| 0    | v       | ∞         | 8,v       | 4,v       | 3,v       | 3,v       | 4,v       |
| 1    | vx      | 11,x      | 8,v       | 4,v       |           | 3,v       | 4,v       |
| 2    | vxu     | 11,x      | 8,v       | 4,v       |           |           | 4,v       | 
| 3    | vxuw    | 11,x      | 8,v       |           |           |           | 4,v       | 
| 4    | vxuwt   | 11,x      | 8,v       |           |           |           |           | 
| 5    | vxuwty  | 11,x      |           |           |           |           |           |
| 6    | vxuwtyz |           |           |           |           |           |           |

**b) Compute the shortest path from *y* to all network nodes.**
| step | N'      | D(z),p(z) | D(v),p(v) | D(w),p(w) | D(x),p(x) | D(u),p(u) | D(t),p(t) |
|------|---------|-----------|-----------|-----------|-----------|-----------|-----------|
| 0    | y       | 12,y      | 8,y       | ∞         | 6,y       | ∞         | 7,y       |
| 1    | yx      | 12,y      | 8,y       | 12,x      |           | ∞         | 7,y       |
| 2    | yxt     | 12,y      | 8,y       | 12,x      |           | 9,t       |           | 
| 3    | yxtv    | 12,y      |           | 12,x      |           | 9,t       |           | 
| 4    | yxtvu   | 12,y      |           | 12,x      |           |           |           | 
| 5    | yxtvuz  |           |           | 12,x      |           |           |           |
| 6    | yxtvuzw |           |           |           |           |           |           |

**c) Compute the shortest path from *w* to all network nodes.**
| step | N'      | D(z),p(z) | D(v),p(v) | D(y),p(y) | D(x),p(x) | D(u),p(u) | D(t),p(t) |
|------|---------|-----------|-----------|-----------|-----------|-----------|-----------|
| 0    | w       | ∞         | 4,w       | ∞         | 6,w       | 3,w       | ∞         |
| 1    | wu      | ∞         | 4,w       | ∞         | 6,w       |           | 5,u       |
| 2    | wuv     | ∞         |           | 12,v      | 6,w       |           | 5,u       | 
| 3    | wuvt    | ∞         |           | 12,v      | 6,w       |           |           | 
| 4    | wuvtx   | 14,x      |           | 12,v      |           |           |           | 
| 5    | wuvtxy  | 14,x      |           |           |           |           |           |
| 6    | yxtvuzw |           |           |           |           |           |           |

## Consider the network shown below, and each node initially knows the cost to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node *z*.
|   | z | x | v |
|---|---|---|---|
| z | 0 | 2 | 6 |
| x | ∞ | ∞ | ∞ |
| v | ∞ | ∞ | ∞ |

## Consider figure 5.7. Suppose there is another router *w*, connected to router *y* and *z*. The costs of all links are given as follows: c(x,y) = 4, c(x,z) = 50, c(y,w) = 1, c(z,w) = 1, and c(y,z) = 3. Suppose that posisoned reverse is used in the distance-vector routing algorithm.
**a) When the distance-vector routing is stabilized, router *w*, *y* and *z* inform their distances to *x* to each other. What distance values do they tell each other?**
|   | z             | w             | y                     |
|---|---------------|---------------|-----------------------|
| z | N/A           | $D_z (x)$ = ∞ | $D_z (x)$ = 4+1+1 = 6 |
| w | $D_w (x)$ = 5 | N/A           | $D_w (x)$ = ∞         |
| y | $D_y (x)$ = 4 | $D_y (x)$ = 4 | N/A                   |

**b) Now suppose that the link cost between *x* and *y* increase to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many interactions are needed for the distance-vector routing to reach a stable state again? Justify your answer.** Simply put, yes we would still run into a count-to-infinity problem even with poisoned reverse being used. The trapped packet would be stuck in a loop between nodes *y*, *z*, and *w*. While I do know that a trapped packet will occur, I do not know how to figure out how many iterations are needed to for the distance-vector routing algorithm to stabalize again.

**c) How do you modify c(y,x) such that there is no count-to-infinity problem at all if c(y,x) changes from 4 to 60?** If we were to remove the connection between *y* and *z* all together, then we would be removing the loop that caused the count-to-infinity problem in the first place when c(y,x) increased to 60. 