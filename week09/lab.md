# Wireshark IP Lab

## Process
I completed the IP Wireshark Lab provided from the Textbook's website and did not deviate from the instructions.

## Lab Questions
**NOTE: for this lab, I had to use the provided Wireshark traces for this lab as I could not send ICMP messages at a specific size**

### Part One: Basic IPv4
#### Select the first UDP segment sent by your computer via the ```traceroute``` command to gaia.cs.umass.edu. Expand the Internet Protocol part of the packet in the packet details window. What is the IP address of your computer?
Upon reviewing the Internet Protocol section of the first UDP packet sent by the provided host, I found that it's source IP address, or the IP address of the provided host, to be 192.168.86.61.

#### What is the value in the time-to-live (TTL) field in this IPv4 datagram’s header?
In the same Internet Protocol section of the first UDP packet, I found that the time-to-live field for this packet was set to 1. This makes sense, as in this situation we are refering to the first packet sent, which begins at time-to-live of 1.

#### What is the value in the upper layer protocol field in this IPv4 datagram’s header?
Again, refering to the Internet Protocol section of the first UDP packet, I found that the upper layer protocol field was set (very obviously) to UDP.

#### How many bytes are in the IP header?
Again, refering to the Internet Protocol section of the first UDP packet, I found that the header length for this packet is 20 bytes. This makes sense as the minimum IPv4 header length is 20 bytes. And after more digging around in the Internet Protocol section of the same packet, I found that there were no IP options, therefore, the IP header length must be the minimum size.

#### How many bytes are in the payload of the IP datagram? Explain how you determined the number of payload bytes.
Now, I calculated that the payload of the IP datagram is 36 bytes. I determined/calculated this number because I had found the overall size of the IP datagram in the Internet Protocol section of the UDP packet to be 56 bytes. From here, I simply took the calculation: ```total IP datagram size - IP datagram header length = IP datagram payload```. Thus, 56 - 20 = 36 bytes.

#### Has this IP datagram been fragmented? Explain how you determined whether or not the datagram has been fragmented.
Refering to the internet Protocol section of the first UDP packet, I found that the fragment offset value for the packet was set to 0. Similary, in the subsequent ICMP packet, I found that the fragment offset value was also set to 0 in the same section. Therefore, I am to reasonably infer that there was no fragmentation with the first IP datagram.

#### Which fields in the IP datagram always change from one datagram to the next within this series of UDP segments sent by your computer destined to 128.119.245.12, via traceroute? Why?
After reviewing the IP datagrams sent from the provided host in sequential order, I found that there were three fields that change: identification, time to live, and checksum. Identification number changes as expected with each IP datagram, and therefore, so does the datagram's checksum as it is dependent on the IP datagram header which includes the identification number. Lastly, time to live changes is changing as traceroute increments subsequent packets.

#### Which fields in this sequence of IP datagrams (containing UDP segments) stay constant? Why?
Again, after reviewing the IP datagrams sent from the provicded host in sequential order, I found that were multiple fields that stayed constant: version, header lenght, differentiated services, protocol, source address, and destination address. These are all constant as we are sending IP datagrams containing UDP segments to a specified host from a specifified host that does not change.

#### Describe the pattern you see in the values in the Identification field of the IP datagrams being sent by your computer.
Again, after reviewing the IP datagrams sent from the provided host in sequential order, I found that the identification field started at some arbitrary number, but incremented by one with each IP datagram, which makes sense to me.

#### What is the upper layer protocol specified in the IP datagrams returned from the routers?
After reviewing the IP datagrams being returned to the provided host, it became quite clear that the upper layer protocol of the returned IP datagrams is ICMP.

#### Are the values in the Identification fields (across the sequence of all of ICMP packets from all of the routers) similar in behavior to your answer to question 9 above?
Again, after reviewing the IP datagrams being returned to the provided host, I found that the value in the identification does behave in similar fashion to my answer in question 9, the only difference is that the starting identification number is different.

#### Are the values of the TTL fields similar, across all of ICMP packets from all of the routers?
Again, after reviewing the IP datagrams being returned to the provided host, I found that the value of the time to live field across the datagrams have been set to 64, which is a common default value for ICMP returning IP datagrams.

### Part Two: Fragmentation
#### Find the first IP datagram containing the first part of the segment sent to 128.119.245.12 sent by your computer via the traceroute command to gaia.cs.umass.edu, after you specified that the traceroute packet length should be 3000. Has that segment been fragmented across more than one IP datagram?
Yes, after looking at the first IP datagram containing the first part of the segment sent by the provided host to gaia.cs.umass.edu, I found that the segment was fragmented across 3 total IP datagrams.

#### What information in the IP header indicates that this datagram been fragmented?
Upon reviewing the first IP datagram, I found under the flag section of the IP header that the "More Fragments" flag was set, therefore, indicating that the datagram being sent was a fragment and there is more fragments to be received by the destination.

#### What information in the IP header for this packet indicates whether this is the first fragment versus a later fragment?
Again, after reviewing the first IP datagram, I found that the fragment offset value was 0, which I can only assume means that this IP datagram is the first fragment. I double checked my assumption with the next IP datagram fragment, which I found that its fragment offset value was set to the size of the first IP datagram fragment, proving my earlier assumption.

#### How many bytes are there in is this IP datagram (header plus payload)?
I found that the size of this IP datagram fragment was 1500 bytes, 1480 byte payload + 20 byte header.

#### Now inspect the datagram containing the second fragment of the fragmented UDP segment. What information in the IP header indicates that this is not the first datagram fragment?
Like above, I found that the fragment offset value of the second IP datagram fragment was set to the payload size of the first IP datagram fragment, suggesting that this is the second IP datagram of the sequence.

#### What fields change in the IP header between the first and second fragment?
After reviewing the second IP datagram fragment, I found two fields that have changed between the two IP datagram headers: fragment offset and header checksum. As we have already discussed, fragment offset has changed between the two IP datagrams, indicating the order of the IP datagrams. And because of this difference in IP header, thus changes the header checksum.

#### Now find the IP datagram containing the third fragment of the original UDP segment. What information in the IP header indicates that this is the last fragment of that segment?
After reviewing the third and final IP datagram fragment, I found within its IP header the that the flag for "More Fragments" was not set this time, indicating that there are no more expected IP datagram fragments.

### Part Three: IPv6
#### What is the IPv6 address of the computer making the DNS AAAA request? Give the IPv6 source address for this datagram in the exact same form as displayed in the Wireshark window.
After reviewing the packet containing the DNS AAAA request, I found that the source IP address was 2601:193:8302:4620:215c:f5ae:8b40:a27a, which is a bit longer than what I am used to with IPv4.

#### What is the IPv6 destination address for this datagram? Give this IPv6 address in the exact same form as displayed in the Wireshark window.
Again, after reviewing the packet containing the DNS AAAA request, I found that the destination IP address was 2001:558:feed::1.

#### What is the value of the flow label for this datagram?
Again, after reviewing the packet containing the DNS AAAA request, I found that the value of the flow label was 0x063ed0. I am assuming that this flow label deals with IPv6's ability to set flow labels on packets for 'special' treatment, however, I do not know what exactly this flow label means.

#### How much payload data is carried in this datagram?
Again, after reviewing the packet containing the DNS AAAA request, I found that the payload was 37 bytes. Which does not make sense to me as I know that the IPv6 header is 40 byte fixed-length, and the total size of the IP datagram was 91 bytes, which does not add up.

#### What is the upper layer protocol to which this datagram’s payload will be delivered at the destination?
Again, after reviewing the packet containing the DNS AAAA request, I found that the upper layer protocol for which the datagram payload was delivered was UDP, which makes sense as we are dealing with DNS.

#### How many IPv6 addresses are returned in the response to this AAAA request?
After reviewing the packet containing the DNS AAAA response, I found that there were two IPv6 addresses returned in response to the AAAA request.

#### What is the first of the IPv6 addresses returned by the DNS for youtube.com? Give this IPv6 address in the exact same shorthand form as displayed in the Wireshark window.
Again, after reviewing the packet containing the DNS AAAA response, I found that the first of the two IPv6 addresses was 2607:f8b0:4006:815::200e.