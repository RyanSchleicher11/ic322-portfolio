# Protocol Pioneer, Act II Chapter I

## Introduction
The objective of this lab was set up a strategy to obtain the best success rate for sending and receiving transmissions between routers when considering interference. And, to be a beta tester of the Protocol Pioneer story game.

## Collaboration
In this lab, I worked with Anuj Sirsikar and Owen Pitchford. But we also got stuck at one point in the lab, and turned to Alex Traynor and Gabe Spencer's help.

## Process
Followed the instructions for [Protocol Pioneer Act II Chapter I](https://gitlab.com/jldowns-usna/protocol-pioneer) from the course website.

## Lab Questions

### Include your client and server strategy code
```
x = math.floor((self.current_tick()%400)/100) + 1
# and the addition of the conditional statement 'if str(x) == self.id:'
if not self.interface_sending(selected_interface):
    if str(x) == self.id:
        if len(self.from_layer_3()) > 0:
            msg_text = self.from_layer_3().pop()
            print(f"{self.id}: Attempting send.")
            self.send_message(msg_text, selected_interface)
```

### Explain your strategies in English
Our first strategy was to split up the current ticks and assigned a number to each client and server and depending on the value of the current tick, a certain client/server would be able to transmit. It made sense to us in theory, but our steady-state success rate was extremely low. Then, we looked at Alex and Gabe's approach and made our own spin on it with different numbers. We chose 400 to mod the current ticks by because we tried out some other numbers first and we got the best results with 400. We thought why not just mod by 4 but we needed to account for enough time for a message to transmit and we figured giving more time would be better.

### What was your maximum steady-state success rate (after 300 or so ticks)?
We found that after 300 ticks or so, our steady-state sucess rate was 0.196. However, we hit a maximum success rate of 0.201 at around 200 ticks.

### Evaluate your strategy. Is it good? Why or why not?
Our first strategy was simply bad, but our second strategy worked well as the steady-state success rate was pretty high, or at least high enough for our liking.

### Are there any strategies you'd like to implement, but you don't know how?
We would have liked to have implemented a slotted ALOHA implementation but we did not know how to. We would also like to find a solution that doesn't just feel like guess and check.

### Any other comments?
No, none that I can think of.