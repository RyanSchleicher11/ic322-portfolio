# Learning Objectives

## I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem.
One of the broad classes of multiple-access protocols are the random-access protocols, which include both the slotted ALOHA and the CSMA/CD protocols. The slotted ALOHA protocol is one of the simplest random access protocols, while CSMA/CD embodies the two important rules of polite human conversation: listening before speaking and if someone else begins talking at the same time, stop talking.

With the slotted ALOHA protocol, we must assume the following: all frames are exactly *L* bits long, time is divided into slots of size *L*/*R* seconds, all nodes are synchronized and start to transmit frames only at the beginning of slots, and if two or more frames collide in a slot, then all the nodes detect the collision event before the slot ends. Now, the operation of slotted ALOHA is quite simple, when a node has a fresh frame to send, it must wait until the beginning of the next slot to transmit the entire frame. If there is a collision, the node retransmits its fram in each subsequent slot with probability *p* until the frame is transmitted without a collision. And a transmission without a collision means a successful transmission.
> ![ALOHA protocol](<learning-objectives-images/ALOHA.jpg>)

Now, with the CSMA/CD protocol, it must follow the two rules outlined above. If a frame from another node is currently being transmitted into the channel, the node wanting to transmit will have to wait until it detects no transmission for a short amount of time before beginning to transmit. And, if a node currently transmitting it must detect that another node is transmitting an interfering frame, and if so, must stop transmitting and wait a random amount of time before it can begin transmitting again.
> ![CSMA/CD protocol](<learning-objectives-images/CSMACD.png>)

## I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC.
Whenever a node wants to send data to another node, it must first augment its data *D* with error-detection and -correction bits *EDC*. Then, both *D* and *EDC* are sent to the receiving node in a link-layer frame. Now, at the receiving end, *D'* and *EDC'* are received. It is important to know that *D'* and *EDC'* may be different than the original *D* and *EDC* as a result of in-transit bit flips. This is where error-detection and -correction come in to help the receiving node determine whether or not *D'* is the same as *D*. Altogether there are four schemes: parity bits, two-dimensional parity, Internet checksum, and CRC.
- Parity bits: the simplist form of error-detection is the use of a single parity bit, which will likely work in an even parity scheme. Here, data *D* has *d* bits plus an additional bit such that its value is set so that the total number of 1s in the *d+1* bits is even. Now, if the receiver finds an odd number of 1s in the received *d+1* bits, then it knows that there was been an odd number of bit errors that have occured.
> ![Parity Bit](<learning-objectives-images/ParityBit.gif>)

- Two-Dimensional parity: a more robust error-dedectiion scheme than the single parity bit. Here, the *d* bits in *D* are broken up into *i* rows and *j* columns, resulting in *i+j*+1 parity bits for the link-layer error-detection bits. Now, the receiver can not only detect that an error has occured, but also able to pinpoint the row and column of the error and correct such error.
> ![Two-Dimensional Parity Bits](<learning-objectives-images/2DParityBits.gif>)

- Interent checksum: the *d* bits in *D* are broken up into sequences of *k*-bit integers, which are summed and used as the error-detection bits. More specifically, the *d* bits in *D* are broken up into 16-bit integers and summed, then the 1s complement of this sum forms the Internet checksum which is carried in the segment header. Now, on the receiver end, it will take the 1s complement of the sum of the data received plus the Internet checksum, and if any of the bits are 1 then the receiver knows an error has occured.

- Cyclic Redundancy Check (CRC): an error-detection scheme used widely today. The sending and receiving node must first agree on an *r*+1 bit pattern, known as a generator *G*, and the most significant bit of *G* be 1. Now, for a given *d* bits in *D*, the sender will choose *r* additional bits and append them to *D* such that the resulting *d+r* bit pattern is exactly divisible by *G* using modulo-2 arithmetic. Therefore, the receiver can divide the *d+r* bits it receives by *G*, if the remainder is nonzero, it knows that an error has occured
> ![CRC](<learning-objectives-images/CRC.gif>)

## I can describe the Ethernet protocol including how it implements each of the Link Layer services and how different versions of Ethernet differ.
In today's day and age, Ethernet is the most prevalent wired Local Access Network (LAN) technology. Ethernet was the first widely deployed high-speed LAN and, therefore, network administrators became very familiar with Ethernet and were very reluctant to have to switch over to a different LAN technology. Moreover, Ethernet produced versions that operated at equal or higher rates than those other competing LAN technologies. For these reasons, Ethernet has more or less won out as the leading LAN technology.

Now, lets take a look at the Ethernet frame, which the sending host interface will encapsulate a given IP datagram into an Ethernet frame to be sent to the receiving host in the same LAN.
> ![Ethernet Frame](<learning-objectives-images/EthernetFrame.png>)

- Preamble: an 8-byte field whose first 7 bytes are 10101010 and last byte is 10101011 meant to essentially wake up the receiving host and synchronize the receiving host's clock to the sending host
- Destination address: contains the MAC address of the destination host
- Source address: contains the MAC address of the sending host
- Type/Length: permits Ethernet to multiplex network-layer protocols
- Data: carries the IP datagram
- CRC: allows the receiving host to detect errors from the received frame

All of the Ethernet technologies provide connectionless service to the network-layer, there is no handshake between the sending and receiving hosts before data is sent over the LAN. Additionally, Ethernet technologies provide an unreliable service to the network-layer, that is, the receiving host will never send an ACK or NAK back to the sending host. This unreliable transport is what enables Ethernet to be simple and cheap, but also means the sending host will have no way of knowing if its data was sent to the receiving host and passed the CRC check. However, this can be fixed via the higher layer application using TCP, which would trigger the sending host to retransmit data to fulfill TCP's reliable data transfer service.

Now, it is important to note that Ethernet is not one technology, but instead comes in many different forms defined by an acronym. These acronyms include 10BASE-T, 100BASE-T, 1000BASE-LX, 10GBASE-T, 10BASE-2, 40GBASE-T, and more. But, what these acronyms have in common are how the acronyms are formed. The first part of the acronym defines the standard speed of the Ethernet technology, such as 10 Mbps, 100 Mbps, 1000 Mbps, 10 Gbps, and 40 Gbps, respectively. The middle part of the acronym, usually "BASE" refers to baseband Ethernet, meaning that the physical media only carries Ethernet traffic. The third and final part of the acronym defines the physicial media the Ethernet technology uses as Ethernet is both a link-layer and physical-layer technology. Ethernet physical medias include coaxial cable (a number), copper wire (T), and fiber (LX).