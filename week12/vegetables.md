# Selected Questions, *Kurose* Chapter Six, Sections 6.1 through 6.4.2

## Suppose two nodes start to transmit at the same time a packet of length *L* over a broadcast channel of rate *R*. Denote the propagation delay between the two nodes as *d_prop*. Will there be a collision if *d_prop* < *L*/*R*? Why or why not?
Put simply, there will be a collision. This is because the propogation delay, or the flight time of the packet, is less than that of the transmission delay, or the time it takes to encode the packet onto the wire, causing both nodes to receive packets from the other node while it is currently transmitting.

## In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?
In regards to slotted ALOHA, it only has three of the four desirable characteristics of a broadcast channel. That is, it is one of the simplest (therefore cheap) multiple access protocols and it allows a node to transmit continuously at the full rate *R* when that node is the only active node, and for more than one node, say *M* nodes, it allows each node an average *R*/*M* transmission rate. The only characterisitc slotted ALOHA lacks is that while it is a decentralized protocol, it requires the time slots across nodes to be synchronized. On the other hand, token passing has all four of the desirable characteristics of a broadcast channel, including being fully decentralized.

## In CSMA/CD, after the fifth collision, what is the probability that a node chooses $K=4$? The result $K=4$ corresponds to a delay of how many seconds on a 10 Mbps Ethernet?
After the fifth collision, the node will choose an integer between 0 and $2^5-1$ (0-31), therefore the probability that the node chooses $K=4$ from 32 numbers is $1/32$. Because the bit time for Ethernet is 512, the node must wait $K*512$ bit time. Additionally, we know that 10 Mbps is equal to $10*1000000$ bps and to get total wait time of the node, we simply take $(K*512)/(10*1000000)$. Now, given that the node chooses $K=4$, we have that $(4*512)/(10*1000000) = 204.8 µs$. 

## Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even bit parity scheme is being used. What would the valie of the field containing the parity bits be for the case of two-dimensional parity scheme?
| packet bits | row bits |  
|-------------|----------|
| 1 1 1 0     |        1 |
| 0 1 1 0     |        0 |
| 1 0 0 1     |        0 |
| 0 1 0 1     |        0 |

| packet bits | column bits |  
|-------------|-------------|
| 1 0 1 0     |           0 |
| 1 1 0 1     |           1 |
| 1 1 0 0     |           0 |
| 0 0 1 1     |           0 |

| packet bits         | parity bit |  
|---------------------|------------|
| 1110 0110 1001 0101 |          1 |

## Suppose the information portion of a packet contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string "Internet". Compute the Internet checksum for this data.
| ASCII symbol | Decimal value | 8-bit binary |
|--------------|---------------|--------------|
| I            | 073           | 0100 1001    |
| n            | 110           | 0110 1110    |
| t            | 116           | 0111 0100    |
| e            | 101           | 0110 0101    |
| r            | 114           | 0111 0010    |
| n            | 110           | 0110 1110    |
| e            | 101           | 0110 0101    |
| t            | 116           | 0111 0100    |

0100 1001 0110 1110 + 0111 0100 0110 0101 = 1011 1101 1101 0011 + 0111 0010 0110 1110 = 0011 0000 0100 0001 + 0110 0101 0111 0100 = 1001 0101 1011 0101. The 1s complement of the sum, 1001 0101 1011 0101, is 0110 1010 0100 1010 which is also the Internet checksum.

## Consider the previous problem (P5), but suppose *D* has the value:
**a) 1000100101.**
> ![P6 Part a Solution](<vegetables-images/P6a.jpg>) 

**b) 0101101010.**
> ![P6 Part b Solution](<vegetables-images/P6b.jpg>)

**c) 0110100011.**
> ![P6 Part c Solution](<vegetables-images/P6c.jpg>)

## Suppose four active nodes, A, B, C, and D, are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability *p*. The first slot is numbered slot 1, and so on.
**a) What is the probability that node A succeeds for the first time in slot 4?** This would be the probability that node A transmits successfully in slot 4 and the other nodes do not transmit in slot 4, and node A failed in the previous 3 slots, therefore, would be $(p*(1-p)^3)*(1-(p*(1-p)^3))$.

**b) What is the probability that some node succeeds in slot 5?** This is the probability that any node transmits successfully in slot 5, therefore, would be $4*(p*(1-p)^3)$.

**c) What is the probability that the first success occurs in slot 4?** This is the probability that any node transmis successfully in slot 4, and all the other nodes failed in the previous 3 slots, therefore, would be $4*(p*(1-p)^3)*(1-(p*(1-p)^3))$.

**d) What is the efficiency of this four-node system?** The efficiency of this system is $4*(p*(1-p)^3)$.

## Consider a broadcast channel with *N* nodes and transmission rate of *R* bps. Suppose the broadcast channel uses polling for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmitted is *d_poll*. Suppose that within a polling round, a given node is allowed to transmit at most *Q* bits. What is the maximum throughput of the broadcast channel?
Because we are given the number of nodes in the broadcast channel, and how much each node is allowed to transmit each polling round, then we know that the maximum amount of data that can be transmitted in any given round is $N*Q$ bits. And, because we are given the broadcast channel transmission rate and the polling delay, then we also know that the maximum time for each round is $N*(d_poll + Q/R)$ seconds. Now that we know the maximum amount of data that can be transmitted in any given round and the time it would take to transmit that maximum amount of data, then we also know the throughput of the broadcast channel is $(N*Q)/(N*(d_poll + Q/R))$ = $Q/(d_poll + Q/R)$ bps.