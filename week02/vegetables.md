# Selected Questions, *Kurose* Computer Networks, Chapter Two

### What information is used by a process running on one host to identify a process running on another host?
There are two things the running process on a host needs to identify a process running on another host: IP address and port number. The IP address is a 32-bit value that uniquely identifies a host. Port number then further identifies the process that is running on the other host.

### List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP ot TCP (or both) provides such service.
1. Reliable data transfer: provide the guarantee that the data sent by one end of the applicaiton is delivered correctly and completely to the other end of the application for applications that require such. TCP offers such service. 
2. Throughput: provide guaranteed available throughput (rate at which the sending process can deliver bits to the recieving process). Neither TCP or UDP offers such service.
3. Timing: provide guaranteed timing. Neither TCP or UDP offer such service.
4. Security: provide an application with one or more security services (i.e. encryption). Neither TCP or UDP offers such service.

### Why do HTTP, SMTP, and IMAP run on top of TCP rather than UDP?
HTTP, SMTP, and IMAP run on top of TCP rather than UDP because TCP provides a reliable data transfer service, while UDP does not. With TCP, it ensures that the data it transmits is intact, correct, and in order (eventually) for the receiving end, which applications like HTTP, SMTP, and IMAP require in order to work properly. While on the other hand, applications like DNS use UDP to achieve speedy answers.

### Consider an e-commerce site that wants to keep a purchase record for each of its customers. Describe how this can be done with cookies.
When a customer contacts the e-commerce site for the first time, the e-commerce site will return a cookie to the customer which will be stored and managed by their browser. This cookie can contain pertinent information for the e-commerce site, such as personal information, browsing history on the site, and purchase records. With this cookie, the e-commerce site can personalize their site to the customer's preferences and previous purchases, and make purchasing items in the future faster and easier.

### Describe how Web caching can reduce the delay in recieving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?
Web caching can reduce the delay in recieving a requested object, if that object is within the web cache. This is because each browser request is first pushed to the web cache, in which it checks to see if the requested object is stored within it. However, if the object is not found within the web cache, then the web cache must act as a client and request the object from its origin server to retrieve back to the original client.

### Telnet into a Web server and send a multiline request message. Include in the request message the ```If-modified-since:``` header line to force a response message with the ```304 Not Modified``` status code.
First, I used this command: ```telnet gaia.cs.umass.edu 80``` to telnet into the server. From here, I then entered in the GET request:
```
GET /wireshark-labs/INTRO-wireshark-file1.html HTTP/1.1
Host: gaia.cs.umass.edu
If-modified-since: Tue, 26 Sep 2023 06:00:00 GMT
```
As a result, I received this HTTP response:
```
HTTP/1.1 304 Not Modified
Date: Tue, 26 Sep 2023 22:44:02 GMT
Server: Apache/2.4.6 (CentOS) OpenSSL/1.0.2k-fips PHP/7.4.33 mod_perl/2.0.11 Perl/v5.16.3
ETag: "51-6063cc58afb46"
```

### In Section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support *n* simultaneous connections, each from a different client host, how many sockets would the TCP server need?
TCP requires an additional socket over UDP because TCP is a connection-oriented protocol. In other words, the client and server need to 'handshake' in order to establish a TCP connection. TCP servers, therefore, have a 'welcome' socket that is always waiting for a connection, and creates a new socket when a connection is made. Therefore, if a TCP server were to support *n* simultaneous connections, each from a different client, then there would exactly *n+1*