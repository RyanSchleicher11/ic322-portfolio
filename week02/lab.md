# Wireshark HTTP Lab

## Process
I completed the HTTP Wireshark Lab provided from the Textbook's website and did not deviate from the instructions.

## Lab Questions
### Part One:
> ![LAB02P1](<lab-images/lab02p1.PNG>)
#### Is your browser running HTTP version 1.0, 1.1, or 2? What version of HTTP is the server running?
My browswer and the server were both running HTTP version 1.1.

#### What languages (if any) does your browser indicate that it can accept to the server?
It appears that the accepted languages is en-US and en, which I am assuming both to be English.

#### What is the IP address of your computer? What is the IP address of the gaia.cs.umass.edu server?
The IP address of my computer is: 10.25.134.241. The IP address of ```gaia.cs.umass.edu``` is: 128.119.245.12.

#### What is the status code returned from the server to your browser?
The status code retuned from the server is 200.

#### When was the HTML file that you are retrieving last modified at the server?
The HTML file was last modified on Thu, 31 Aug 2023 05:59:01 GMT.

#### How many bytes of content are being returned to your browser?
128 bytes are being returned to my browser.

#### By inspecting the raw data in the packet content window, do you see any headers within the data that are not displayed in the packet-listing window? If so, name one.
No, I do not see any headers within data that are not displated in the packet-listing window.

### Part Two:
> ![LAB02P2](<lab-images/lab02p2.PNG>)
#### Inspect the contents of the first HTTP GET request from your browser to the server. Do you see an “IF-MODIFIED-SINCE” line in the HTTP GET?
No, I do not see an "IF-MODIFIED-SINCE" line in the first HTTP GET request.

#### Inspect the contents of the server response. Did the server explicitly return the contents of the file? How can you tell?
Yes, the server responded with a status code of 200 OK, which means nothing went wrong. Additionally, the server sent 371 bytes of data which I assume to be its contents.

#### Now inspect the contents of the second HTTP GET request from your browser to the server. Do you see an “IF-MODIFIED-SINCE:” line in the HTTP GET? If so, what information follows the “IF-MODIFIED-SINCE:” header?
Yes, there is an "IF-MODIFIED-SINCE" line in the second HTTP GET request, and is followed by ```If-Modified-Since: Thu, 31 Aug 2023 05:59:01 GMT```.

#### What is the HTTP status code and phrase returned from the server in response to this second HTTP GET? Did the server explicitly return the contents of the file? Explain.
This time, the server responded with a status code of 304 Not Modified. Therefore, I would assume that the server did not explicitly return the contents of the file because we had already recieved the contents of the file from the server with the first HTTP GET request and because the file was not modified since the time given.

### Part Three:
> ![LAB02P3](<lab-images/lab02p3.PNG>)
#### How many HTTP GET request messages did your browser send? Which packet number in the trace contains the GET message for the Bill or Rights?
My browser only sent one HTTP GET request, which happened to be packet number 117.

#### Which packet number in the trace contains the status code and phrase associated with the response to the HTTP GET request?
My HTTP GET request response occured at packet 127.

#### What is the status code and phrase in the response?
The response status code was 200 and phrase was OK.

#### How many data-containing TCP segments were needed to carry the single HTTP response and the text of the Bill of Rights?
A total of four data-containing TCP segments were needed to carry the single HTTP response and Bill of Rights text.

### Part Four:
> ![LAB02P4](<lab-images/lab02p4.PNG>)
#### How many HTTP GET request messages did your browser send? To which Internet addresses were these GET requests sent?
There were a total of three HTTP GET request messages that were sent by my browser. The first HTTP GET request was sent to 128.119.245.12, the base file. The second HTTP GET request was also sent to 128.119.245.12, but for the Pearson logo. The third HTTP GET request was sent to 178.79.137.164, the 8th Edition textbook cover.

#### Can you tell whether your browser downloaded the two images serially, or whether they were downloaded from the two web sites in parallel? Explain.
I would argue that my browser downloaded the two images serially, because the HTTP GET request for the Pearson logo .png file and its subsequent HTTP OK response both occured before the HTTP GET request for the 8th Edition texbook cover .jpg file.

### Part Five:
> ![LAB02P5](<lab-images/lab02p5.PNG>)
#### What is the server’s response (status code and phrase) in response to the initial HTTP GET message from your browser?
The server's response to the initial HTTP GET request message was 401 Unauthorized.

#### When your browser sends the HTTP GET message for the second time, what new field is included in the HTTP GET message?
In the second HTTP GET request message, the new field ```Authorization: Basic d2lyZXNoYXJrLXN0dWRlbnRzOm5ldHdvcms=``` is added.