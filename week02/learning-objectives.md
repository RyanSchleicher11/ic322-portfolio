# Learning Objectives

## I can explain the HTTP message format, including the common fields.
There are two types of HTTP messages: request message and response message.

> ![General format of an HTTP request message image](<learning-objectives-images/HTTPrequest.jpg>)

The request line is broken up into three key fields: method, URL, and version. The method field can take on several values, such as GET (the most common value), POST, HEAD, PUT, and DELETE. The URL field identifies the location of the object being requested. And the version field specifies the HTTP version. The header lines are meant to contain more information about the object being requested. Finally, the entity body can be used by methods that need additional information from form fields for the object being requested.

> ![General format of an HTTP response message image](<learning-objectives-images/HTTPresponse.jpg>)

The status line is broken up into three key fields: version, status code, and phrase. The version field specifies the version of HTTP is running. The status code and phrase fields both indicate the result of the request, such as ```200 OK```, ```400 Bad Request```, and ```404 Not Found```. The header lines are meant to contain more information about the response, such as its location or information about the server providing the requested object. Finally, the entity body contains contents of the requested object.

## I can explain the difference between HTTP GET and POST requests and why you would use each.
HTTP GET requests are used for whenever the browser requests an object. On the other hand, HTTP POST requests are used for whenever the user fills out a form, and often used when submitting a login or a form, or even when uploading a file to a server. As result, HTTP POST requests do include information within the entity body of the HTTP request message, while HTTP GET requests do not.

## I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.
A GET request is considered a conditinal GET request if the request includes an ```If-Modified-Since:``` header line, for which its value is set to the time at which the requested object was first downloaded by the client. In short, conditional GET requests solve the issue of 'stale' objects residing within cache. Here is an example of a conditional GET request:
```
GET /wireshark-labs/INTRO-wireshark-file1.html HTTP/1.1
Host: gaia.cs.umass.edu
If-modified-since: Tue, 5 Aug 2023 01:47:48 GMT
```
Now, if the object has not been modified since the ```If-Modified-Since:``` time, then the Web server returns a HTTP response message containing ```304 Not Modified``` in the status line. Otherwise, the Web server will return a HTTP response message containing ```200 OK``` in the status line.

## I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.
The HTTP response code and its phrase are the result of the HTTP request. Some common HTTP response codes and phrases include:
- ```200 OK```: request suceeded and object's information is returned
- ```301 Moved Permanently```: requested object moved to a new URL provided by the ```Location:``` header line, and client software will retrieve the new URL
- ```400 Bad Request```: generic error code that the request could not be understood by the server
- ```404 Not Found```: requested object does not exist on the specified server
- ```500 Version Not Supported```: requested HTTP version is not supported by the server

## I can explain what cookies are used for and how they are implemented.
At the most basic level, cookies are used to allow sites to keep track of user information. Cookies have four components when you first visit a site using cookies:
1. The cookie header line ```Set-cookie: xxxx``` within the HTTP response message to be saved within the browser's special file
2. A cookie header line ```Cookie: xxxx``` within the HTTP request message on subsequent visits that the browser obtains from its cookie file
3. A cookie file on your end system and managed by the browser, obviously
4. A back-end database at the site

## I can explain the significance of HTTP pipelining.
HTTP pipelining, specifically with HTTP/1.1 persistent connections, the web server leaves the original TCP connection to the web server open. Therefore, subsequent requests and responses between the same client and server can be sent over that original TCP connection (i.e. an entire web page), which can be incredibly faster than creating a new TCP connection after every request a client sends.

## I can explain the significance of CDNs, and how they work.
Content Distribution Networks (CDNs) are simply a network of geographically distributed caches. And because CDNs are caches, they help to localize traffic as a way to reduce response time to requests and reduce costs of upgrading bandwidth. Today, CDNs are popular amongst major video-streaming companies (Netflix and YouTube) because video files are both very popular and large in size. There are two ways in which CDNs can work: enter deep and bring home. Through Enter Deep philosophy, CDN server clusters are deployed to access ISPs all over the world, the goal is to get close to the end systems. Through the bring home philosophy, larger CDN server clusters are instead typically deployed at IXPs, and typically result in lower maintenance.
