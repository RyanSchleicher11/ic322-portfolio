# Wireshark DNS Lab
## Process
I completed the DNS Wireshark Lab provided from the Textbook's website and did not deviate from the instructions.

## Lab Questions
### Part One
#### Run nslookup to obtain the IP address of the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in. What is the IP address of www.iitb.ac.in?
After entering ```nslookup www.iitb.ac.in``` into a terminal, I found that the IP address of www.iitb.ac.in is 103.21.124.10.

#### What is the IP address of the DNS server that provided the answer to your ```nslookup``` command in question 1 above?
Again, after entering ```nslookup www.iitb.ac.in``` into a terminal, I found that the IP address of the DNS server that provided the answer is 127.0.0.53.

#### Did the answer to your ```nslookup``` command in question 1 above come from an authoritative or non-authoritative server?
According to the screenshot, my answer from question 1 came from a non-authoritative server. I found that this is the I am using my local DNS server to ```nslookup```. If I wanted an authoritative answer, I would have to explicitly specify the authoritative DNS server within the ```nslookup``` command.

#### Use the nslookup command to determine the name of the authoritative name server for the iit.ac.in domain. What is that name? (If there are more than one authoritative servers, what is the name of the first authoritative server returned by nslookup)? If you had to find the IP address of that authoritative name server, how would you do so?
According to the screenshot, there are three authoritative name servers for the iitb.ac.in: dns1.iitb.ac.in, dns2.iitb.ac.in, and dns3.iitb.ac.in. If I wanted to find the address of dns1.iitb.ac.in, I could simply use the command ```nslookup dns1.iitb.ac.in``` in a terminal.

### Part Three, Section A
// screenshot of DNS query message
#### Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number in the trace for the DNS query message? Is this query message sent over UDP or TCP?
Upon examining the DNS query message, the packet number is 423, which has no other meaning besides just locating it and where it occured during the packet capture timeline. But, what is more interesting is that the DNS query message was sent over UDP, which according to [GeeksforGeeks](https://www.geeksforgeeks.org/why-does-dns-use-udp-and-not-tcp/) is because DNS queries are often short and require quick responses.

#### Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message? Is this response message received via UDP or TCP?
Upon examining the DNS query message, the packet number immediately follows its DNS query number at 424, which is not very surprising considering DNS requires a quick response. And again, the DNS response message was also sent over UDP, which I assume to be for the same reason as stated above.

#### What is the destination port for the DNS query message? What is the source port of the DNS response message?
The destination port for the DNS query message and the source port of the DNS response message both match at port number 53. This is because port number 53 is dedicated to DNS activities involving UDP.

#### To what IP address is the DNS query message sent?
The  DNS query message was sent to IP address 10.1.74.10, which after some investigating is the IP address of my local DNS server.

#### Examine the DNS query message. How many “questions” does this DNS message contain? How many “answers” does it contain?
Upon examining the DNS query message, there is 1 question and 0 answers. To find 0 answers is to be expected as we are refering to a query and not a response, and the 1 question is refering to the number of query entries (e.i. gaia.cs.umass.edu).

#### Examine the DNS response message to the initial query message. How many “questions” does this DNS message contain? How many “answers” does it contain?
Like above, upon examining the DNS response message, there is 1 question and 1 answer. The 1 answer was to be expected as it provides the information about the requested host, however, what I did not understand was the 1 question. After asking ChatGPT, I found that the DNS server utilizes this question field to ensure that the correct information is provided in response to the specific question being asked.

#### What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/? What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address? What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E2.jpg? What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.
The initial HTTP GET request for the base file was found at packet number 432. The DNS query message to resolve gaia.cs.umass.edu was found at packet number 423, and its subsequent DNS response message can be found immediately following at packet number 424. The HTTP GET request for the image object can be found at packet number 470, and the only DNS query message to resolve gaia.cs.umass.edu is the original DNS query message at packet number 423. This is because DNS caching temporarily stores information about recent DNS searches (i.e. gaia.cs.umass.edu), and because we had already resolved gaia.cs.umass.edu, its information had been stored in the DNS cache to be used for the image object HTTP GET request.

### Part Three, Section B
#### What is the destination port for the DNS query message? What is the source port of the DNS response message?
Like the similar question above, the destination port for the DNS query message and the source port of the DNS response message both match at port number 53. Again, this is because port number 53 is dedicated to DNS activities involving UDP.

#### To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
The DNS query message was sent to IP address 10.1.74.10, which is the IP address of my default local DNS server. This doesn't surprise me as all DNS queries are first sent to their local DNS server before going through the DNS server hierarchy if need be.

#### Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?
Upon examining the DNS query message, the DNS query is of type A, which means the query is asking for the domain address. And, there are 0 answers, which makes sense as it is a DNS query and not a DNS response.

#### Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?
Like above, upon examining the DNS response message, there is 1 question and 1 answer. The 1 answer was to be expected as it provides the information about the requested host, and as we know from above, the 1 question is for the DNS server to ensure the proper answer is given to the specified question.

### Part Three, Section C
#### To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?
Like above, the DNS query message was sent to IP address 10.1.74.10, which is the IP address of my default local DNS server.

#### Examine the DNS query message. How many "questions" does the query have? Does the query message contain any “answers”?
Upon examining the DNS query message, there is 1 question and 0 answers. Again, to find 0 answers is to be expected as we are refering to a query and not a response, and the 1 question is refering to the number of query entries (e.i. umass.edu).

#### Examine the DNS response message. How many answers does the response have? What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?
Upon examining the DNS response message, there are 3 answers and 3 additional resource records. Both the answer section and the additional resource record section contain the same general information, the difference lies in that the answer section answers the specific question in that umass.edu was resolved in type NS format, whereas in the additional resource record section resolved umass.edu in type A format.