# Selected Questions, *Kurose* Computer Networks, Chapter Two

## Suppose Alice, with a Web-based e-mail account sends a message to Bob, who accesses his mail server using IMAP. Discuss how the message gets from Alice's host to Bob's host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.
There are three clear steps which Alice's message takes to get to Bob. First, the message will be sent from Alice's host to her mail server via the HTTP protocol. Then, the message will be sent from Alice's mail server to Bob's mail server via the SMTP protocol. Finally, Bob will then be able to access the message Alice sent (on his host) via the IMAP protocol.

## What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?
As we can recall, HTTP/1.1 utilizes persistent TCP connections (HTTP pipelining). While we found that this single TCP connection is faster than creating a new TCP connection for each request, some objects may run into an issue (called HOL blocking) where a larger object is taking a long time to get through a bottleneck link and subsequnetly delays smaller objects that are behind it. HTTP/2 attempts to fix this issue by breaking each message into small frames, and interleaving the request/response messages on the same TCP connection.

## CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them.
1. Enter Deep: the aim is to *enter deep* by positioning CDN server clusters in access ISPs all over the world with the goal of getting close to end users
2. Bring Home: the aim is to *bring the ISPs home* by building large clusters of CDNs at a smaller number of sites (usually IXPs), and typically result in lower maintenance costs

## How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.
SMTP marks the end of a message body by a line consisting of a single period, while HTTP does not specifically mark the end of its message body. Instead, HTTP has the header line ```Content-Length:```, which for HTTP/1.1 persisent connection can be utilized to find the end of the message body. While it may seem that HTTP can mark the end of its message body the same as SMTP, it cannot. This is because HTTP can send data other than person-friendly ASCII, while SMTP is strictly ASCII.

## What is a *whois* database? Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used.
According to [whois.com](https://www.whois.com/whois/):
> A Whois domain lookup allows you to trace the ownership and tenure of a domain name. Similar to how all houses are registered with a governing authority, all domain name registries maintain a record of information about every domain name purchased through them, along with who owns it, and the date till which it has been purchased.
I used whois.com to obtain these two DNS servers: ```ns1.google.com``` and ```ns1.yahoo.com```

## Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the Web servers (outside your department) that are most popular among the users in your department? Explain.
Given that we have access to the caches in the local DNS servers, we could periodacly check the cache over a defined period of time. If a web server shows up multiple times within the cache, as the cache holds recently and frequently visited web servers, then that means that the web server is popular among users within the department.

## Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user. Can you determine if an external Web site was likely accessed from a computer in your department a couple seconds ago? Explain.
Now, given the we do not have access to the caches in the local DNS servers, we have to use an alternate method to determine if an external website was likely accessed a couple seconds ago. This alternate method involves using the ```dig``` command (very similar to ```nslookup```) and utilizing its ```Query time:``` field. If the query time for a specified address is less than a few microseconds, then it could reasonbly assumed that that address was accessed a couple seconds ago.