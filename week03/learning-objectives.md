# Learning Objectives

## I can explain how the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.
No single DNS server has all of the IP addresses in the Internet, there is simply way too many. Therefore, DNS uses a large number of servers organized in a hierarchical fashion and positioned around the world. There are three tiers of DNS servers: root DNS servers, top-level domain (TLD) servers, and authoritative DNS servers.
- Root DNS servers: top-most level DNS server that provides the IP addresses of TLD servers
- TLD servers: there is a TLD server for all major top-level and country domains that provide the IP addresses for authoritative DNS servers
- Authoritative DNS servers: houses all IP addresses and its associated host of an organizations publicly accessible hosts (i.e. web server or mail server)

Now, the local DNS server does not belong to the hierarchy described above, but still plays a very important role. Each ISP has a local DNS server that when a host makes a DNS query, is first sent to their local DNS before it forwards the query through the hierarchy.

## I can explain the role of each DNS record type.
There are four types of DNS record: A, NS, CNAME, and MX.
- ```Type=A```: provides the standard hostname-to-IP address mapping
- ```Type=NS```: provides the nameserver of the DNS server that is an authoritative server for the given hostname
- ```Type=CNAME```: provides querying host the canonical (standard) name for a hostname
- ```Type=MX```: allow the hostnames of mail servers to have simple aliases (a company may have the same aliased name for its mail server and for one of its other servers like a web server)

## I can explain the role of the SMTP, IMAP, and POP protocols in the email system.
- Simple Mail Transfer Protocol (SMTP): the principal application-layer protocol for electronic mail, utilizing TCP in order to transfer mail from a host's (sender) mail server to another host's (reciever) mail server
- Internet Mail Access Protocol (IMAP): allows a client to access and manipulate their electronic mail messages from their mail server
- Post Office Protocol (POP): very similar to IMAP in that it also allow a client's host to access mail from their mailbox server, according to [RFC 918](https://www.rfc-editor.org/rfc/rfc918)

## I know what each of the following tools are used for: ```nslookup```, ```dig```, ```whois```.
- ```nslookup```: according to its man page (```man nslookup```), ```nslookup``` is a program to query DNS servers, and can be used to either query name servers for information about various hosts and domains or to just print the name and requested information for a host or domain
- ```dig```: according to its man page (```man dig```), ```dig``` is very similar to ```nslookup``` in that it performs DNS lookups and displays the answers that are returned from the name server(s) that were queried
- ```whois```: according to [whois.com](https://www.whois.com/whois/), ```whois``` allows you to trace the ownership and tenure of a domain name, which is very similar to both ```nslookup``` and ```dig```, but offers a bit more information