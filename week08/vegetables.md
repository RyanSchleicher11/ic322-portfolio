# Selected Questions, *Kurose* Chapter Four, Sections 4.3.3 and 4.3.4

## Visit a host that uses DHCP to obtain its IP address, network mask, default router, and IP address of its local DNS server. List these values.
I used my own host to obtain these values and performed the actions as listed from the DHCP wireshark lab. As such, after looking at the DHCP ACK message, I found that my host obtained the IP address 10.25.134.241, network mask 255.255.192.0, default router 10.25.128.1, and local DNS server 10.1.74.10.

## Suppose there are three routers between a source host and a destination host. Ignoring fragmentation, an IP datagram sent from the source host to the destination host will travel over how many interfaces? How many forwarding tables will be indexed to move the datagram from the source to the destination?
Simply put, the IP datagram will travel over 8 total interfaces and indexed over 3 forwarding tables. This is because each router has two interfaces (WAN and LAN side) plus one interface for each host, thus, 2*3 + 2 = 8. And, because the IP datagram will be traveling through 3 routers, it will be indexed over a forwarding table for each router it passes over.

## What is a private network address? Should a datagram with a private network address ever be present in the larger public Internet? Explain.
First, a private network address is an internal network address behind the NAT router. As we have discussed earlier, there are only so many addresses with IPv4 and even fewer private network addresses, therefore, their could be multiple hosts around the world using the same private network address simultaneously. Secondly, a datagram with a private network address should neber be present within the larger public Internet, because the private network address may be used by multiple hosts within their private networks around the world.

## Consider the topology shown in Figure 4.20. Denote the three subnets with hosts as Networks A, B, and C. Denote the subnets without hosts as Networks D, E, and F.
**a) Assign network addresses to each of these six subnets, with the following constraints: All addresses must be allocated from 214.97.254/23; Subnet A should have enough addresses to support 250 interfaces; and both Subnet B and C should have enough addresses to support 120 interfaces each. Of course, Subnets D, E, and F should each be able to support 2 interfaces.**
| Subnet | Addresses                                                    |
|--------|--------------------------------------------------------------|
| A      | 214.97.254.0/24 (2^8 = 256 addresses)                        |
| B      | 214.97.255.8/25 - 214.97.255.0/29 (2^7-2^3 = 120 addresses)  |
| C      | 214.97.255.128/25 (2^7 = 128 addresses)                      |
| D      | 214.97.255.0/31 (2^1 = 2 addresses)                          |
| E      | 214.97.255.2/31 (2^1 = 2 addresses)                          |
| F      | 214.97.255.4/30 (2^2 = 4 addresses)                          |

**b) Using your answer to part (a), provide the forwarding tables for each of the three routers.**

**Router 1:**
| Destination Address Range | Link Interface |
|---------------------------|----------------|
| 214.97.254.0/24           | A              |
| 214.97.255.0/31           | D              |
| 214.97.255.4/30           | F              |

**Router 2:**
| Destination Address Range | Link Interface |
|---------------------------|----------------|
| 214.97.255.128/25         | C              |
| 214.97.255.2/31           | E              |
| 214.97.255.4/30           | F              |

**Router 3:**
| Destination Address Range         | Link Interface |
|-----------------------------------|----------------|
| 214.97.255.0/25 - 214.97.255.0/29 | B              |
| 214.97.255.0/31                   | D              |
| 214.97.255.2/31                   | E              |

## Use the whois service at the American Registry for Internet Numbers to determine the IP address blocks for three universities. Can the whois services be used to determine with certainty the geographical location of a specific IP address? Use www.maxmind.com to determine the locations of the Web servers at each of these universities.
After using the ARIN whois service, I have found that the IP address block for Washington State University is 192.94.21.0 - 192.94.21.255, the IP address block for Oregon State University is 128.193.0.0 - 128.193.255.255, and the IP address block for California Baptist University is 12.204.91.0 - 12.204.91.255. And no, the ARIN whois service cannot determine with certainty the geographical location of a specific IP address. Then, after using www.maxmind.com, I found that the Oregon State University web servers are in Bend, Oregon, the Washington State University web servers are in Pullman, Washington, and the California Baptist University web servers are in Riverside, California.

## Consider the network setup in Figure 4.25. Suppose that the ISP instead assigns the router the address 24.34.112.235 and that the network address of the home network is 192.168.1/24.
**a) Assign the address to all interfaces in the home network.** We are given three interfaces on the home network. We can simply assign the addresses 192.168.1.1, 192.168.1.2, and 192.168.1.3 to these three interfaces.

**b)Suppose each host has two ongoing TCP connections, all to port 80 at host 128.119.40.86. Provide the six corresponding entries in the NAT translation table.** Given the router IP address and arbitrary port numbers, we can produce the following NAT Translation Table:
| WAN                  | LAN                |
|----------------------|--------------------|
| 24.34.112.235 / 4001 | 192.168.1.1 / 4050 |
| 24.34.112.235 / 4002 | 192.168.1.1 / 4051 |
| 24.34.112.235 / 5001 | 192.168.1.2 / 5050 |
| 24.34.112.235 / 5002 | 192.168.1.2 / 5051 |
| 24.34.112.235 / 6001 | 192.168.1.3 / 6050 |
| 24.34.112.235 / 6002 | 192.168.1.3 / 6051 |

## Suppose you are interested in detecting the number of hosts behind a NAT. You observe that the IP layer stamps an identification number sequentially on each IP packet. The identification number of the first IP packet generated by a host is a random number, and the identification numbers of the subsequent IP packets are sequentially assigned. Assume all IP packets generated by hosts behind the NAT are sent to the outside world.
**a) Based on this observation, and assuming you can sniff all packets sent by the NAT to the outside, can you outline a simple technique that detects the number of unique hosts behing a NAT? Justify your answer.** In this situation, I propose that we simply count the number of unique patterns outlayed from the sniffed packets. This can be done because each identification number following the first IP packet is sequential, therefore, follows a pattern. So, if we were to count the number of patterns (i.e. the chain of suquential packets), then we can reasonably assume that this also translated to the number of hosts behind the NAT.

**b) If the identification numbers are not sequentially assigned but randomly assigned, would your technique work? Justify your answer.** Simply put, no. The clear pattern laid out by sequential numbering would no longer exist, consequently, we cannot rely on randomly assigned identification numbers to accurately count the number of hosts behind a NAT.