# Wireshark NAT Lab 
## Process
I completed the NAT Wireshark Lab provided from the Textbook's website and did not deviate from the instructions.

## Lab Questions
### What is the IP address of the client that sends the HTTP GET request in the nat-inside-wireshark-trace1-1.pcapng trace? What is the source port number of the TCP segment in this datagram containing the HTTP GET request? What is the destination IP address of this HTTP GET request? What is the destination port number of the TCP segment in this datagram containing the HTTP GET request?
Upon reviewing the HTTP GET request from the inside NAT wireshark trace, I found that the source IP address to be 192.168.10.11, the source port number to be 53924, the destination IP address to be 138.76.29.8 and the destination port number to be 80. I'm sure these fields will be important to compare later with the outside NAT wireshark trace.

### At what time is the corresponding HTTP 200 OK message from the webserver forwarded by the NAT router to the client on the router’s LAN side?
I found that the time of the corresponding HTTP 200 OK message from the webserver to be at 0.030672101 seconds.

### What are the source and destination IP addresses and TCP source and destination ports on the IP datagram carrying this HTTP 200 OK message?
After reviewing the HTTP 200 OK message from the inside NAT wireshark trace, I found that the source IP address to be 138.76.29.8, the source port number to be 80, the destination IP address to be 192.168.10.11, and the destination port number to be 53924. This makes sense as it matches with my answer to question one.

### At what time does this HTTP GET message appear in the nat-outside-wireshark-trace1-1.pcapng trace file?
I found that the HTTP GET message appears at time 0.027356291 seconds.

### What are the source and destination IP addresses and TCP source and destination port numbers on the IP datagram carrying this HTTP GET (as recorded in the nat-outside-wireshark-trace1-1.pcapng trace file)?
Upon reviewing the HTTP GET request from the outside NAT wireshark trace, I found that the source IP address to be 10.0.1.254, the source port number to be 53924, the destination IP address to be 138.76.29.8, and the destination port number to be 80. These answers are different from question one, which makes sense since we are going outside the NAT router.

### Which of these four fields are different than in your answer to question 1 above?
Specifically, I found that the source IP address was different from my answer above and my answer to question one, which again makes sense.

### Are any fields in the HTTP GET message changed?
After reviewing both HTTP GET requests, I could not find any differences in the fields section.

### Which of the following fields in the IP datagram carrying the HTTP GET are changed from the datagram received on the local area network (inside) to the corresponding datagram forwarded on the Internet side (outside) of the NAT router: Version, Header Length, Flags, Checksum?
After reviewing each of the four listed fields and comparing the two types of NAT wireshark traces, I found that the checksum value changed. This is because the checksum depends on the source IP address, and because the source IP changed between the two traces, the checksum value also had to change as well.

### At what time does this message (HTTP 200 OK message) appear in the nat-outside-wireshark-trace1-1.pcapng trace file?
I found that the HTTP 200 OK message appears at time 0.030625966 seconds.

### What are the source and destination IP addresses and TCP source and destination port numbers on the IP datagram carrying this HTTP reply (“200 OK”) message (as recorded in the nat-outside-wireshark-trace1-1.pcapng trace file)?
After reviewing the HTTP 200 OK message from the outside NAT wireshark trace, I found that the source IP address to be 138.76.29.8, the source port number to be 80, the destination IP address to be 10.0.1.254, and the destination port number to be 53924.

### What are the source and destination IP addresses and TCP source and destination port numbers on the IP datagram carrying the HTTP reply (“200 OK”) that is forwarded from the router to the destination host in the right of Figure 1?
Although this IP datagram cannot be viewed witin the outside NAT wireshark trace, after having learned about NAT and seeing the inside NAT wireshark trace, the IP datagram in this case can be reasonbly put together. It would look exactly like the HTTP 200 OK message from the inside NAT wireshark trace as NAT simply replaces the destination IP address and port number with the correct host's information before forwarding it on. Therefore, the source IP address would be 138.76.29.8, the source port number would be 80, the destination IP address would be 192.168.10.11, and destination port number would be 53924.