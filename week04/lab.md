# TCP Wireshark Lab

## Process
I completed the TCP Wireshark Lab provided from the Textbook's website and did not deviate from the instructions.

## Lab Questions
### NOTE: I used the provided trace file from the lab website as I could not gather a good/useful trace of my own.
### Part Two
#### What is the IP address and TCP port number used by the client computer (source) that is transferring the alice.txt file to gaia.cs.umass.edu?
After looking at the TCP packets, and using the HTTP packets for confirmation, I found that the IP address of the source computer to be 192.168.86.68 and the used TCP port number to be port number 55639.

#### What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?
Again, after looking at the TCP packets, and using the HTTP packets for confirmation, I found that the IP address of gaia.cs.umass.edu to be 128.119.245.12 and it is sending/recieving TCP segments on port number 80.

### Part Three
#### What is the sequence number of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? What is it in this TCP segment that identifies the segment as a SYN segment? Will the TCP receiver in this session be able to use Selective Acknowledgments (allowing TCP to function a bit more like a “selective repeat” receiver, see section 3.4.4 in the text)?
First, I found that the sequence number of the TCP SYN segment was 0 for this trace. And upon looking further into the TCP SYN segment, I found that the segment was flagged with hexadecimal 0x002, which upon more digging into the TCP segment, set the SYN flag to 1. And yes, the TCP receiver in this session will be able to use Selective Acknowledgments, as the SACK TCP option has been permitted.

#### What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is it in the segment that identifies the segment as a SYNACK segment? What is the value of the Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu determine that value?
First, I found that the sequence number of the TCP SYNACK segment sent by gaia.cs.umass.edu was 0 for this trace. And upon looking further into the TCP SYNACK segment, I found that the segment was flagged with hexadecimal 0x012, which upon more digging of the TCP segment, set both the SYN flag to 1 and the ACK flag to 1. Finally, I found that the value of the acknowledgement field in the TCP SYNACK segment was 1. The value of the acknowledgment field is determined by gaia.cs.umass.edu as it is the byte in the stream that is expected next (also shows which bytes have been received).

#### What is the sequence number of the TCP segment containing the header of the HTTP POST command? How many bytes of data are contained in the payload (data) field of this TCP segment? Did all of the data in the transferred file alice.txt fit into this single segment?
First, I found that the sequence number of the TCP segment containing the header of the HTTP POST command was 1. Upon looking further into the TCP segment, I found that the data field contains 1434 bytes of data, which should be extremely obvious that all of the data in the alixe.txt file DID NOT fit into this single TCP segment.

#### Consider the TCP segment containing the HTTP POST request as the first segment in the data transfer part of the TCP connection.
**At what time was the first segment (the one containing the HTTP POST) in the data-transfer part of the TCP connection sent?** The first TCP segment was sent at time t = 0.024047.
**At what time was the ACK for this first data-containing segment received?** The ACK for this first TCP segment was recieved at time t = 0.052671.
**What is the RTT for this first data-containing segment?** RTT = ACK recieved time - sent time, RTT = 0.052671 - 0.024047 = 0.028624.
**What is the RTT value the second data-carrying TCP segment and its ACK?** RTT = ACK recieved time - sent time, RTT = 0.052676 - 0.024048 = 0.028628.
**What is the EstimatedRTT value (see Section 3.5.3, in the text) after the ACK for the second data-carrying segment is received?** EstimatedRTT = 0.875*0.028624 + 0.125*0.028628 = 0.028624.

#### What is the length (header plus payload) of each of the first four data-carrying TCP segments?
Each of the four data-carrying TCP segments are 1514 bytes in length, 1448 bytes of TCP payload, which means the remaining 66 bytes is for the TCP header.

#### What is the minimum amount of available buffer space advertised to the client by gaia.cs.umass.edu among these first four data-carrying TCP segments? Does the lack of receiver buffer space ever throttle the sender for these first four data-carrying segments?
Upon looking at the SYNACK packet, the minimum amount of available buffer space advertised by gaia.cs.umass.edu was 28960 bytes. However, when looking at the subsequent packets, specifically the data-carrying TCP segments, the mimimum amount of available buffer space advertised by gaia.cs.umass.edu grew to a constant 131712 bytes. Finally, because this mimimum amount of available buffer space is so high, I would not imagine that the sender is ever throttled, at least for the first four data-carrying TCP segments.

#### Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?
It does not appear as though there were any retransmited TCP segments in the trace. I came to this conclusion because I looked through the sequence numbers of the TCP segments being sent and as I went down the list of TCP segments, all the sequence numbers were increasing at a constant rate, and nothing was out of order.

#### How much data does the receiver typically acknowledge in an ACK among the first ten data-carrying segments sent from the client to gaia.cs.umass.edu? Can you identify cases where the receiver is ACKing every other received segment (see Table 3.2 in the text) among these first ten data-carrying segments?
Upon, examining the trace file, it appears as though among the first ten data carrying TCP segments, the receiver typically ackownledges 1448 bytes of data in an ACK. This makes sense as the payload of each data-carrying TCP segment to gaia.cs.umass.edu is 1448 bytes of data. Finally, during my examination of the trace file, it does not appear as though the receiver is ACKing every other received segment as each ACK is exactly 1448 greater than the last.

#### What is the throughput (bytes transferred per unit time) for the TCP connection? Explain how you calculated this value.
For this problem, I am going to use total amount of bytes received (difference between the acknowledged sequence number of the last ACK and first sequence number of the TCP segment) divided by the total time of transmission (difference between the last ACK received and the first TCP segment sent). Therefore, we get to: (153426-1) / (0.191486-0.024047) = 916303.85 bps = 916.3 kbps.

### Part Four
#### Use the *Time-Sequence-Graph(Stevens)* plotting tool to view the sequence number versus time plot of segments being sent from the client to the gaia.cs.umass.edu server. Consider the “fleets” of packets sent around t = 0.025, t = 0.053, t = 0.082 and t = 0.1. Comment on whether this looks as if TCP is in its slow start phase, congestion avoidance phase or some other phase. Figure 6 shows a slightly different view of this data.
Upon examining the provided graph from the lab instructions, it appears as though at time t = 0.025, TCP is in its slow start phase, and at time t = 0.1, TCP is in its congestion avoidance phase. At time t = 0.025, there aren't very many segments being sent as compared to other "fleets", which is why I argue that it is in its slow start phase. Then, at time t = 0.1, there seems to be close short bursts of segments that are being sent, which I can only assume to be congestion avoidance. As for time t = 0.053, it appears that there is a small amount of congestion control as there is a secondary burst of a couple segments that sent. And finally for time t = 0.7, I am unsure of this "fleet", there is a large amount of segments that were sent off all within a very small amount of time, and am unsure of what phase to call this.

#### These “fleets” of segments appear to have some periodicity. What can you say about the period?
Again, upon examining the provided graph, it appears as though that from times t = 0 through t = 0.1, the period of "fleets" is 0.025. But, beyond time t = 0.1, the period between "fleets" shortens down to t = 0.0125. I believe this is because at time t = 0.1, the slow start phase has ended completely, and therefore can send segments more periodically.

#### Answer each of two questions above for the trace that you have gathered when you transferred a file from your computer to gaia.cs.umass.edu
I had trouble gathering a good/useful TCP trace of my own, therefore, I do not have a trace of my own to examine.