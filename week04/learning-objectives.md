# Learning Objectives
## I can explain how TCP and UDP multiplex messages between processes using sockets.
In short, multiplexing is the job of gathering chunks of data from different sockets on the host, encapsulating each chunk of data with its proper header information to create segments (UDP or TCP), and passing the newly created segment down to the network layer.

Say a prcoess on Host A (with a specified port number x) wants to send a chunk of data to a process on Host B (also, with a specified port number y). The transport layer in Host A will create a UDP segment defined by the tuple (source port x, destination port y). Then, the transport layer passes this UDP segment off to the network layer which encapsulates the segment into a datagram and sends it off to Host B.

Now, say Host A (with a specified port number x and IP address a) wants to initiate a HTTP session with Server B (with a specified IP address b). The transport layer in Host A will create a TCP segment defined by the 4-tuple (source port x, source IP a, destination port 80, destination IP b). Then, the transport layer passes this TCP segment off to the network layer which encapsulates the segment into a datagram and sends it off to Server B.

## I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.
UDP is the most basic transport layer protocol. All it does is take a message from the application layer, attach the source and destination port number field, two other small fields, and passes the segment further along down the chain. When the UDP segment is received and reached the transport layer, UDP simply uses the desintation port number to send the segment to the correct application process. While it may seem that TCP is always prefered over UDP, as it is connectionless and unreliable, is not necessarily true. Here are a few examples:
- Finer application-level control over what data is sent, and when: because UDP packages data into a UDP segment and immediately passes the segment along to the network layer, real-time applications that require a minimum sending rate tend to prefer this aspect of UDP
- No connection establishment: UDP does not introduce any delay to establish a connection, an aspect prefered by DNS
- No connection state: UDP does not maintain a connection state between end systems, and therefore an application that runs over UDP can support more active clients at the same time
- Small packet header overhead: in every segment, UDP only has 8 bytes of overhead, while TCP has 20 bytes

TCP, on the otherhand, is said to be connection-oriented. That is, before two processes begin to send/receive data, they must first undergo what's known as the three-way handshake to establish a 'connection' for communication. Once the connection has been established, then when a host wants to send data, their data is directed to the connection's send buffer before it is encoded with TCP specific information and passed along to the network-layer for transmission. When the receiving host receives the sent TCP segment, it is placed in the TCP conneciton's receive buffer for which the receiving host's application can read from. TCP provides multiple services, here are a few examples:
- Full-duplex: in a TCP connection, data can flow between Host A and Host B, and vice versa, at the same time
- Point-to-point: a TCP connection is always between a single sender and a single receiver, no more
- Reliable data transfer: ensures that the data stream that a process application reads is uncorrupted, without gaps, without duplication, and in sequence
- Flow/Congestion control: flow control essentially a speed-matching service between the rate the sender is sending information and the rate the receiver application is reading data; congestion control essentially throttles the sender's rate of sending depending on congestion within the IP network

## I can explain how and why the following mechanisms are used and which are used in TCP: sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.
- Sequence Numbers: used by the TCP sender and receiver in implementing a reliable data transfer service, and accounts for the possibility that the ACK or NAK packet from the receiver could have been corrupted or dropped
- Duplicate ACKs: an ACK that reacknowledges a segment for which the sender has already received, and can be used to detect if a packet was lost even before the timer has run out of time
- Timers: helps the sender detect if a packet has been lost/dropped during transmission after a given time, and if time expires following a transmission, the sender will be interrupted to retransmit
- Pipelining: allows the sender to send multiple packets without waiting for their subsequent acknowledgements, allowing the utilization of the sender to increase
- Go-Back-N (GBN): very similar to pipelining in that it is a protocol that allows the sender to send multiple packets without waiting for their acknowledgements, but is constrained to have no more than some maximum allowable number *N* of unacknowledges packets in the pipeline
- Selective Repeat: avoids unnecessary retransmissions by having the sender retransmit only those packets that it suspects were received in error at the receiver, which requires the receiver to individually acknowledge correctly received packets
- Sliding Window: given that the range of sequence numbers that have been transmitted but not yet acknowledged is known as a window of size *N*, then as the GBN protocol operates, this window slides forward over the sequence number space

## I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.
- Sequence Numbers: the receiver only needs to cross-check sequence numbers of the packets it receives to determine whether or not a received packet is a duplicate packet, or if an acknowledgement has been corrupted/dropped
- Duplicate ACKs: can help a sender detect that the packet following the packet that had been ACKed twice had been dropped along transmission
- Timers: can help a sender determine (when time expires) if a packet or acknowledgement has been dropped during transmission
- Pipelining: simply increases utilization
- Go-Back-N (GBN): the receiver accepts packets that are in-order and returns ACKs for the most recently received in-order packet, and discards all other packets as a way to avoid out-of-order packets
- Selective Repeat: helps to fix out-of-order packets as these kind of packets are buffered until the missing packets are received
- Sliding Window: simply a tool in which GBN utilizes