# Week One Learning Objectives

## I can explain the role that the network core plays vs the network edge.
The network edge is comprised by end systems, to include desktop computers, servers, and mobile devices. The network edge holds the responsibility of connecting it's millions of end systems to the Internet so that these end systems can fulfill their role of hosting/running application programs.

The network core, on the otherhand, is comprised of packet-switches and links that are operated by varying levels of ISPs. The network core holds the responsibility of interconnecting end systems and efficiently moving data.

## I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.
- Dial-up: a type of Internet access that uses existing phone cables and services. If users wish to connect, they would have to call a phone number and allow the two modems on either end of the phone line 'talk' to each other. A downside to dial-up is that when this process of 'talking' is taking place, no one else can use the phone line.
- Digital Subscriber Line (DSL): a type of access that like dial-up uses existing phone cables, but is a bit more sophisticated. If the user wishes to connect, their home DSL modem takes digital data and translates it to high-frequency tones for transmission through the existing telephone lines, and translated back into digital format to be sent into the network core using a DSLAM located at the telephone company. DSL allows both data and traditional voice to be transmitted at the same time using different frequencies, allowing the use of the modem and calling simultaneously.
- Cable: a type of access very similar to DSL, except uses the existing television lines and subsequently offered by the television company. If the user wishes to connect, like DSL, cable modems are required to translate digital data into an analog signal through the cable lines (also split between data and television/video), and translated back into digital data to be sent into the network core using a CMTS located at the television company. A downside of cable is that it is a shared broadcast medium.
- Fiber to the Home (FTTH): a new and up-and-coming access that provides higher speeds than dial-up, DSL, and cable. FTTH simply provides an optical path, usually along existing telephone/television paths, from a company's central office directly to the home.
- Wireless: 3G LTE, 4G, and 5G wirelessly connect end systems, from great distances, via a base station operated by a cellular network provider that send digital data directly to the network core. Satellite internet links between ground stations to offer worldwide access. There are two types of satellites: geostationary satellites and low-earth orbiting (LEO) satellites. Geostationary sattellites offer greater coverage as they are further away, but subsequently incur subsantial propogation delay. LEO satellites fix some of this propogation delay, but require a greater number of satellites for the same amount of coverage.

## I can use queuing, transmission, and propagation delays to calculate total packet delay, I can describe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.
- Transmission delay: the time in which it takes to encode a packet of *L* bits onto a wire with a transmission rate of *R* bps: t = L/R, and can be reduced via decreasing the packet size, or increasing transmission rate.
- Propogation delay: the time required to propogate (travel) from the beginning of the link to its destination given the distance *d* and propogation speed *s*: t = d/s, and can be reduced via shortening the distance of wires and/or between hosts.
- Queuing delay: caused by a packet waiting to be transmitted, usually as result of traffic build-up, and its length is determined by the number of already queued packets. Queuing delay can be improved by improving transmission delay and/or propogation delay as either will speed up the time it takes for a packet to reach its destination and therefore, number of packets waiting in a given queue.

Total packet delay: t = L/R + d/s + queuing delay.

## I can describe the differences between packet-switched networks and circuit-switched networks.
In packet-switched networks, the resources needed along a path to allow for the flow of data between end systems is not reserved, instead it is available for everyone to use at the same time. Additionly, data is broken into smaller chunks (packets) that are sent when ready. However, because resources are shared, if a given link as multiple users wanting to send a packet at the same time, some may have to wait until the other transmissions are done (queuing delay).

In curcuit-switched networks on the other hand, these same resources along the data path are reserved for the duration of transmission. This means that bandwidth and access are guarunteed for the user currently transmitting, but could also be inefficient if the user reserving the data path is not actively transmitting data.

## I can describe how to create multiple channels in a single medium using FDM and TDM.
Through Frequency-Division Multiplexing (FDM), the frequency spectrum of a link is divided up into circuits. Essentially, each circuit gets a fraction of the bandwidth continuously.

Through Time-Division Multiplexing (TDM), time is divided into frames of fixed length, and each of these frames are further divided into time slots. When a connection is established, a time slot in every frame is dedicated to that connection. Essentially, each circuit gets all the bandwidth at periodic bursts of time.

## I can describe the hierarchy of ISPs and how ISPs at different or similar levels interact.
There are essentially three tiers of the ISP hierarchy: Access ISPs, Regional ISPs, and Tier-1 ISPs.

- Access ISPs can provide either wired or wireless connectivity, using a wide range of access technologies I have outlined above. Access ISPs aren't just telephone or cable companies, universities and companies providing access to their employees are also classified as Access ISPs.
- Regional ISPs provide access to the Access ISPs in its region (hence regional), and on rare occasions offer access directly to homes.
- Tier-1 ISPs provide access to the Regional ISPs and form the spine for Internet access to the world. There are around a dozen of these Tier-1 ISPs.

As can be seen from the hierarchy of ISPs, lower tiered ISPs (customer ISP) purchase Internet access from the next tier up (provider ISP). To ensure that lower tiered ISPs are more resilient and maintain Internet access to their customers, they may likely purchase access from multiple higher tiered ISPs, commonly known as multi-homing.

Furthermore, ISPs at similar levels of the ISP hierarchy may choose to peer, that is to connect to each other so that both ISPs traffic passes over the shared connection, as to method to save money. Similarly, third-party companies may create Internet Exchange Points (IXPs) to do essentially the same thing as peering but charge a small amount for network accessibility.

## I can explain how encapsulation is used to implement the layered model of the Internet.
At its core, encapsulation is the process of concatenating layer-specific headers to a message for transmitting over the networks.

Say we are given a message at the Application Layer (topmost layer) which we want to send to some destination host. The Application Layer gives this message to the Transport layer for which it appends its own header information (application information and error-detection) to the message, now referred to as the Transport-Layer Segment. From here, the Transport Layer hands the segment off to the Network Layer for which further appends its own header information (source and destination addresses), now referred to as the Network-Layer Datagram. From here, the Network Layer sends the datagram to the Link Layer for which appends the final header information to the datagram, now reffered to as the Link-Layer Frame. Finally, the frame is sent to the Physical Layer for which the entire frame is encoded onto the physical wire and ready to be transmitted. The tranmission may experience intermediate hops by link-layer switches or routers, but should eventually reach its destination host in which the encapsulation process now happens in reverse all the way back up to the Application Layer.