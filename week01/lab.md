# Wireshark Introduction Lab

## Process
I completed the "Getting Started" Wireshark Lab provided from the Textbook's website and did not deviate from the instructions.

## Lab Questions
### Which of the following protocols are shown as appearing in your trace file: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?
I saw many TCP, HTTP, TLSv1.2 and DNS protocols. I also so other protocols not listed, such as TLSv1.3.

### How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received?
The HTTP GET message was logged at time 5.804936s and the HTTP OK reply was logged at 5.827611s. Therefore, the time it took to recieve the HTTP OK reply was about 0.022675s or 22.675ms.

### What is the Internet address of the gaia.cs.umass.edu (also known as www-net.cs.umass.edu)?  What is the Internet address of your computer?
The IP Address of ```gaia.cs.umass.edu``` is 128.119.245.12.
The IP Address of my computer is 10.25.134.241.

### What type of Web browser issued the HTTP request?
My User-Agent showed: ```User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36\r\n```

### What is the destination port number to which this HTTP request is being sent?
The HTTP request was sent to port 80, the port assigned to HyperText Transfer Protocol (HTTP) unencrypted web pages, according to [Technopedia](https://www.techopedia.com/definition/15709/port-80#:~:text=Port%2080%20is%20the%20port,and%20receive%20unencrypted%20web%20pages).

### Print the two HTTP messages. (GET and OK)
```
Frame 132: 527 bytes on wire (4216 bits), 527 bytes captured (4216 bits) on interface \Device\NPF_{99087E5D-07F4-4A4D-A792-1396269ADF55}, id 0
Ethernet II, Src: IntelCor_15:cf:a7 (d0:3c:1f:15:cf:a7), Dst: ArubaaHe_11:71:80 (b8:d4:e7:11:71:80)
Internet Protocol Version 4, Src: 10.25.134.241, Dst: 128.119.245.12
Transmission Control Protocol, Src Port: 50016, Dst Port: 80, Seq: 1, Ack: 1, Len: 473
    Source Port: 50016
    Destination Port: 80
    [Stream index: 5]
    [Conversation completeness: Complete, WITH_DATA (31)]
    [TCP Segment Len: 473]
    Sequence Number: 1    (relative sequence number)
    Sequence Number (raw): 584907001
    [Next Sequence Number: 474    (relative sequence number)]
    Acknowledgment Number: 1    (relative ack number)
    Acknowledgment number (raw): 3710104666
    0101 .... = Header Length: 20 bytes (5)
    Flags: 0x018 (PSH, ACK)
    Window: 512
    [Calculated window size: 131072]
    [Window size scaling factor: 256]
    Checksum: 0x0882 [unverified]
    [Checksum Status: Unverified]
    Urgent Pointer: 0
    [Timestamps]
    [SEQ/ACK analysis]
    TCP payload (473 bytes)
Hypertext Transfer Protocol
    GET /wireshark-labs/INTRO-wireshark-file1.html HTTP/1.1\r\n
    Host: gaia.cs.umass.edu\r\n
    Connection: keep-alive\r\n
    Upgrade-Insecure-Requests: 1\r\n
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36\r\n
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7\r\n
    Accept-Encoding: gzip, deflate\r\n
    Accept-Language: en-US,en;q=0.9\r\n
    \r\n
    [Full request URI: http://gaia.cs.umass.edu/wireshark-labs/INTRO-wireshark-file1.html]
    [HTTP request 1/2]
    [Response in frame: 136]
    [Next request in frame: 180]
```

```
Frame 136: 492 bytes on wire (3936 bits), 492 bytes captured (3936 bits) on interface \Device\NPF_{99087E5D-07F4-4A4D-A792-1396269ADF55}, id 0
Ethernet II, Src: ArubaaHe_11:71:80 (b8:d4:e7:11:71:80), Dst: IntelCor_15:cf:a7 (d0:3c:1f:15:cf:a7)
Internet Protocol Version 4, Src: 128.119.245.12, Dst: 10.25.134.241
Transmission Control Protocol, Src Port: 80, Dst Port: 50016, Seq: 1, Ack: 474, Len: 438
    Source Port: 80
    Destination Port: 50016
    [Stream index: 5]
    [Conversation completeness: Complete, WITH_DATA (31)]
    [TCP Segment Len: 438]
    Sequence Number: 1    (relative sequence number)
    Sequence Number (raw): 3710104666
    [Next Sequence Number: 439    (relative sequence number)]
    Acknowledgment Number: 474    (relative ack number)
    Acknowledgment number (raw): 584907474
    0101 .... = Header Length: 20 bytes (5)
    Flags: 0x018 (PSH, ACK)
    Window: 237
    [Calculated window size: 30336]
    [Window size scaling factor: 128]
    Checksum: 0x8fdb [unverified]
    [Checksum Status: Unverified]
    Urgent Pointer: 0
    [Timestamps]
    [SEQ/ACK analysis]
    TCP payload (438 bytes)
Hypertext Transfer Protocol
    HTTP/1.1 200 OK\r\n
    Date: Thu, 24 Aug 2023 14:50:35 GMT\r\n
    Server: Apache/2.4.6 (CentOS) OpenSSL/1.0.2k-fips PHP/7.4.33 mod_perl/2.0.11 Perl/v5.16.3\r\n
    Last-Modified: Thu, 24 Aug 2023 05:59:01 GMT\r\n
    ETag: "51-603a4ecc02178"\r\n
    Accept-Ranges: bytes\r\n
    Content-Length: 81\r\n
    Keep-Alive: timeout=5, max=100\r\n
    Connection: Keep-Alive\r\n
    Content-Type: text/html; charset=UTF-8\r\n
    \r\n
    [HTTP response 1/2]
    [Time since request: 0.022675000 seconds]
    [Request in frame: 132]
    [Next request in frame: 180]
    [Next response in frame: 183]
    [Request URI: http://gaia.cs.umass.edu/wireshark-labs/INTRO-wireshark-file1.html]
    File Data: 81 bytes
Line-based text data: text/html (3 lines)
```