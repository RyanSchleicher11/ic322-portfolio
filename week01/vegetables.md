# Selected Questions, *Kurose* Computer Networks, Chapter One

## What is the difference between a host and an end system? List several different types of end systems. Is a Web server an end system?
Simply put: there is no difference between host and end system. The textbook states:
> Throughout this textbook we will use the terms hosts and end systems interchangeably; that is, *host = end system*.

Examples of end systems include desktop computers (PCs), mobile decives (smartphones and tablets), and servers to include web servers.

## List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access.
1. Digital Subscriber Line (DSL): home access
2. Cable Internet Access: home access
3. Ethernet: home and enterprise access
4. 5G: wide-area wireless access

## Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2, respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queuing, propagation delay, and processing delay)
At time t0 = 0s, the sending host begins to transmit. At time t1 = L/R1s, the entire packet has been recieved and stored by the switch (no propogation delay) and begins to transmit to the recieving host. At time t2 = t1 + L/R2s, the entire packet has been recieved by the recieving host. Total time is L/R1 + L/R2 seconds.

## What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?
In circuit-switched newtorks, the resources needed for communication between end systems are reserved with a defined throughput, whereas with packet-switched networks, the resources needed for communication are shared amongst other end systems and may result in queuing delays.

With Time-Division Multiplexing (TDM), each circuit gets all the bandwidth periodically during a specified time slot, whereas with Frequency-Division Multiplexing (FDM), each circuit gets a fraction of the bandwidth continuously.

## Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20 percent of the time. (See the discussion of statistical multiplexing in Section 1.3)
**a) When circuit switching is used, how many users can be supported?** Users share a 2 Mbps link, because users transmit at 1 Mbps, only 2 users may be supported simultaneously.

**b) For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queuing delay before the link if two or fewer users transmit at the same time? Why will there be a queuing delay if three users transmit at the same time?**
If two or fewer users transmit simultaneously, then there will be enough bandwidth and thus no queuing delay. Now, if three or more users transmit simultaneously, then there will not be enough bandwidth and consequently queuing delays.

**c) Find the probability that a given user is transmitting.** From the problem: 0.2.

**d) Suppose now there are three users. Find the probability that at any given time, all three users are transmitting simultaneously. Find the fraction of time during which the queue grows.** Probability of all three: 0.2^3 = 0.008, which is also the fraction of time during which the queue grows.

## Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?
Peering at the same hierarchical level allows the customer ISPs to reduce it's costs with it's provider ISPs. According to [cloudfare](https://www.cloudflare.com/learning/cdn/glossary/internet-exchange-point-ixp/), IXPs make money by charging a fee to ISPs when they transmit across their networks.

## How long does it take a packet of length 1000 bytes to propagate over a link of distance 2500 km, propagation speed 2.5 · 10^8 m/s, and transmission rate 2 Mbps? More generally, how long does it take a packet of length *L* to propagate over a link of distance *d*, propagation speed *s*, and transmission rate *R* bps? Does this delay depend on packet length? Does this delay depend on transmission rate?
The time (delay) it takes for this packet to propogate over the link depends on queuing delay, transmission delay, and propogation delay.

In this problem, we will assume no queuing delay given only the single packet to transmit.

Transmission delay and propogation delay are defined as t = L/R and t = d/s, repectively.

Total delay = L/R + d/s = 1000/2*10^6 + 2500*1000/2.5*10^8 = 10.5ms.

## Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R1 = 500 kbps, R2 = 2 Mbps, and R3 = 1 Mbps.
**a) Assuming no other traffic in the network, what is the throughput for the file transfer?** Assuming these links follow in sequential order (Host A -> R1 -> R2 -> R3 -> Host B), the throughput is confined to R1 = 500 kbps (the bottleneck link).

**b) Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B?** t = F/R1 = (4*10^6)/(500*10^6) = 8s.

**c) Repeat (a) and (b), but now with R2 reduced to 100 kbps.** Throughput now becomes R2 = 100 kbps. t = F/R2 = (400*10^6)/(100*10^3) = 40s.