# Selected Questions, *Kurose* Computer Networks, Chapter Three

## Suppose two TCP connections are present over some bottleneck link of rate R bps. Both connections have a huge file to send (in the same direction over the bottleneck link). The transmission of the files start at the same time. What transmission rate would TCP like to give to each of the connections?
Due to TCP fairness (R/K, K being the number of TCP connections), the transmission rate TCP would like to give to both connections in this situation is R/2 bps. However, TCP connections with a smaller RTT are able to grab the avialable link bandwidth more quickly as it becomes available, and therefore, enjoy higher throughput than the other TCP connections with larger RTTs.

## True or false? Consider congestion control in TCP. When the timer expires at the sender, the value of ```ssthresh``` is set to one half of its previous value.
False. When the timer expires at the sender end, the value of ```ssthresh``` is not set to one half its previous value, but is set to half the ```cwnd``` (congestion window) value when congestion was detected.

## Host A and B are communicating over a TCP connection, and Host B has already received from A all bytes up through byte 126. Suppose Host A then sends two segments to Host B back-to-back. The first and second segments contain 80 and 40 bytes of data, respectively. In the first segment, the sequence number is 127, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgement whenever it receives a segment from Host A.
**a) In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?** For the second segment, its sequence number is 207, source port number is 302, and destination port number is 80.

**b) If the first segment arrives before the second segment, in the acknowledgement of the first arriving segment, what is the acknowledgement number, source port number, and the destination port number?** If the first segment arrives before the second segment, then in the acknowledgement, its acknowledgement number is 207, source port number is 80, and destination port number is 302.

**c) If the second segment arrives before the first segment, in the acknowledgement of the first arriving segment, what is the acknowledgement number, source port number, and the destination port number?** Now, if the second segment arrives before the first segment, then in the acknowledgement, its acknowledgement number is 127, source port number is 80, and destination port number is 302.

**d) Suppose the two segments sent by A arrive in order at B. The first acknowledgment is lost and the second acknowledgement arrives after the first timeout interval. Draw a timing diagram, showing these segments and all other segments and acknowledgements sent. For each segment in your figure, provide the sequence number and number of bytes of data; for each acknowledgement that you add, provide the acknowledgement number.**

> ![written representation](<vegetables-image/IMG-0090.jpg>)

## In section 3.5.3, we discussed TCP's estimation of RTT. Why do you think TCP avoids measuring SampleRTT for retransmitted segments?
Say a packet is first transmitted and its timer expires, causing the sender to retransmit that same packet. But, then a acknowledgement for that packet has been received. The problem is, which packet is the acknowledgement actually acknowledging? The first packet that was sent? Or the retransmission? Therefore, to avoid this ambiguity, TCP avoids measuring SampleRTT for retransmitted segments.

## In section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?
It is not uncommon that packets arrive to the receiver out-of-order. Therefore, to avoid unecessary retransmission of packets, TCP waits for three duplicate ACKs, which is a strong indication that a packet was dropped along the way to the receiver.

## Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer.
**a) Identify the intervals of time when TCP slow start is operating.** It appears as though TCP slow start is occuring between times 1-6 and 23-26, as it looks likes the congestion window size starts slow and exponentially increases.

**b) Identify the intervals of time when TCP congestion avoidance is operating.** It appears as though TCP congestion avoidance is occuring between times 6-23, as it looks like the congestion window linearly increases, and periodically cuts itself when a segment loss event occurs.

**c) After the 16th transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?** It appears as though that after the 16th transmission round, the segment loss was detected via a triple duplicate as TCP did not enter its slow start phase.

**d) After the 22nd transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?** Now, it appears as though that after the 22nd transmission round, the segment loss was detected via a timeout as TCP did enter its slow start phase.

**e) What is the initial value of ```ssthresh``` at the first transmission round?** It appears as though the initial value of ```ssthresh``` is 32, as it is at this value that congestion avoidance takes over.

**f) What is the value of ```ssthresh``` at the 18th transmission round?** It would appear that the value of ```ssthresh``` at the 18th transmission round is 21, half that of the congestion window before the segment loss event.

**g) What is the value of ```ssthresh``` at the 24th transmission round?** It would appeat that the value of ```ssthresh``` at the 24th transmission round is 14, half that of the congestion window before the segment loss event.

**h) During what transmission round is the 70th segment sent?** After counting the number of transmission per transmission round, I found that the 70th segment was sent during the 7th transmission round.

**i) Assuming a packet loss is detected after the 26th round by the receipt of a triple duplicate ACK, what will be the values of the congestion window size and of ```ssthresh```?** Following a packet loss event via a triple duplicate ACK, the subsequent value for ```ssthresh``` will be 4 (half the value of ```cwnd``` before the packet loss event) and the value for ```cwnd``` will be 7 (half the value of ```cwnd``` before the packet loss event plus 3 for the triple duplicate ACK).

**j) Suppose TCP Tahoe is used, and assume that triple duplicate ACKs are received at the 16th round. What are the ```ssthresh``` and the congestion window size at the 19th round?** Now, considering that we are in TCP Tahoe, following the (or any) packet loss event at the 16th transmission round, the ```cwnd``` value will now be set to 1 and the ```ssthresh``` value will still be set to 21.

**k) Again suppose TCP Tahoe is used, and there is a timout event at 22nd round. How many packets have been sent out from 17th round till 22nd round, inclusive?** From above, we know that the ```cwnd``` value will be reset to 1, and we also know that TCP slow start will begin. Therefore, starting at round 17 through round 22, the number of packets sent will be 1+2+4+8+16+21 = 52 packets.