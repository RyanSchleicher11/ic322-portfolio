# Selected Questions, *Kurose* Chapter Five, Sections 5.4 through 5.6

## How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?
The NEXT-HOP attribute is the IP address of the router interface that begins the AS-PATH. Now, the AS-PATH attribute contains the list of ASs through which the advertisement has passed. When a prefix is passed to an AS, the AS adds its ASN to the existing list in the AS-PATH.

## True or False: when a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all its neighbors. Explain.
False. This is because BGP is a policy-based rouitng protocol, and therefore, decided by a network administrator. It can be decided that the BGP routers within their network choose not to add their own identity to the received path and send that new path on to all its neighbors. It can also be decided that the BGP routers within their network do not even advertise that new path to all or any of their neighbors in the first place.

## Name four different types of ICMP messages.
1. Type 0 - echo reply
2. Type 3 - destination unreachable
3. type 8 - echo request
4. type 11 - time (ttl) expired

## What two types of ICMP messages are received at the sending host executing the ```traceroute``` program?
The sending host executing the ```traceroute``` program will receive several type 11 ICMP messages and only a very few type 3 (code 3) ICMP messages. As listed above, type 11 ICMP messages are for time (ttl) expiring, which occurs frequently with the ```traceroute``` program and is how the sending host is able to identify intermediate hops. Then the type 3 (code 3) ICMP messages come from the packets actually reach the destination host and signals to the ```traceroute``` to terminate.

## Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is *no* physical link between AS2 and AS4.
> ![P14network](<vegetables-images/P14network.png>)

**a) Router 3c learns about prefix x from which routing protocol: OSPF, RIP, eBGP, or iBGP?** eBGP, because it is a border router that learns of prefix x from router 4c in AS4.

**b) Router 3a learns about x from which routing protocol?** iBGP, because it learns of prefix x from router 3b inside the same AS.

**c) Router 1c learns about x from which routing protocol?** eBGP, because it is a border router that learns of prefix x from router 3a in AS3.

**d) Router 1d learns about x from which routing protocol?** iBGP, because it learns of prefix x from either router 1a or 1b inside the same AS.

## Referring to the previous problem, once router 1d learns about x it will put an entry (x, I) in its forwarding table.
**a) Will I be equal to I_1 or I_2 for this entry? Explain why in one sentence.** I will be equal to I_1 , becuase it is in the direction of the path with the least cost to prefix x.

**b) Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that x is accessible via AS2 as well as via AS3. Will I be set to I_1 or I_2? Explain why in one sentence.** I will now be set to I_2 , because it is in the direction of the new path with the least bost to prefix x.

**c) Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that x is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to I_1 or I_2? Explain why in one sentence.** I will now be set back to I_1 , because it is in the direction of the smaller AS-PATH, and therefore, least bost path to prefix x.

## In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?
> ![Figure 5-13](<vegetables-images/figure5-13.png>)

In this case, ISP A would advertise to ISP B AS-PATHs AW and AV, and ISP A would also advertise to ISP C AS-PATH AV. Therefore, ISP C receives the AS routes: AV, BAW, and BAV. Essentially, through peering, we have gained the ability that for any traffic through ISP C destined for network W must be routed through ISP B in order to arrive at network W.

## Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y’s traffic but does not want to transit X’s traffic. Does BGP allow Z to implement this policy?​
Simply put, yes. BGP allows for AS Z to advertise to Y that it will transit all of its traffic and AS Y to advertise to X that it has no path to Z. So even though AS X and Y are connected and have a peering agreement, with this specific BGP configuration, AS X will never transit traffic to Y.